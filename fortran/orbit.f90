!  -*-f90-*-  (for emacs)    vim:set filetype=fortran:  (for vim)
!
!> @author
!> Konsta Sarkimaki
!>
!> Orbit integration module to evaluates particle position using
!> VPA (volume preserving algorithm) by R. Zhang (2015).
!<
module orbit_mod

  private
  real*8, parameter :: c = 299792458.D0 ![m/s] ! Speed of light in vacuum

  integer, private, parameter :: ORBIT_NANRESULT = -1

  public :: orbit_push, orbit_halfpush, orbit_error

contains

  !> Computes new position and momentum of a test particle in an electromagnetic
  !> field after a given time.
  !> 
  !> input:
  !> 
  !> real*8 ma        -- test particle mass [kg]
  !> real*8 qa        -- test particle charge [C]
  !> real*8 E0half(3) -- electric field at x(t0+0.5*dt) [V/m]
  !> real*8 B0half(3) -- magnetic field at x(t0+0.5*dt) [T]
  !> real*8 dt        -- time step length [s]
  !> real*8 xhalf(3)  -- test particle position at t = t0+0.5*dt (see orbit_halfpush) [m]
  !> real*8 uin(3)    -- normalized test particle momentum u=p/mc at t = t0 [1]
  !>
  !> output:
  !>
  !> real*8 xout(3)   -- test particle position at t = t0 + dt [m]
  !> real*8 uout(3)   -- normalized test particle momentum u=p/mc at t = t0 + dt [1]
  !> integer err      -- error flag, negative indicates something went wrong
  !<
  subroutine orbit_push(ma,qa,E0half,B0half,dt,xhalf,xout,uin,uout,err)
    implicit none
    real*8, intent(in) :: ma
    real*8, intent(in) :: qa
    real*8, intent(in) :: B0half(3)
    real*8, intent(in) :: E0half(3)
    real*8, intent(in) :: dt
    real*8, intent(in) :: xhalf(3)
    real*8, intent(in) :: uin(3)
 
    real*8, intent(out) :: xout(3)
    real*8, intent(out) :: uout(3)
    integer,intent(out) :: err

    
    real*8, dimension(3, 3) :: I = &
         reshape((/1.D0,0.D0,0.D0,&
                   0.D0,1.D0,0.D0,&
                   0.D0,0.D0,1.D0/), shape(I))

    real*8 :: sigma, d, dd(3,3)
    real*8 :: uminus(3), uplus(3), Bhat(3,3)

!!$    real*8 :: uxB(3), udiff(3) ! For testing

    sigma = qa*dt/(2*ma*c)
   
    uminus = uin+sigma*E0half
    d = sigma*c/sqrt(1.D0+dot_product(uminus,uminus))

    Bhat = transpose(reshape((/ &
                0.D0,  B0half(3),  -B0half(2),&
          -B0half(3),       0.D0,   B0half(1),&
           B0half(2), -B0half(1),        0.D0 &
          /), shape(Bhat)))
    dd = 2*d*Bhat/(1.D0+d**2*dot_product(B0half,B0half))
    uplus = matmul(I + dd + d*matmul(dd,Bhat),uminus)

    uout = uplus + sigma*E0half
    xout = xhalf + c*dt*uout/(2*sqrt(1.D0+dot_product(uout,uout)))

    err = 0
    if(any(isnan(xout)) .or. any(isnan(uout))) err = ORBIT_NANRESULT

!!$    !! This is the implicit method which can be used to test that the explicit method is correctly implemented
!!$    uxB(1) = (uin(2)+uout(2))*B0half(3) - (uin(3)+uout(3))*B0half(2)
!!$    uxB(2) = (uin(3)+uout(3))*B0half(1) - (uin(1)+uout(1))*B0half(3)
!!$    uxB(3) = (uin(1)+uout(1))*B0half(2) - (uin(2)+uout(2))*B0half(1)
!!$
!!$    udiff = (dt*qa/(c*ma))*(E0half+c*uxB/sqrt(4.D0+norm2(2*uin+qa*dt*E0half/(ma*c))**2 ))
!!$
!!$    print*, uin
!!$    print*, uout-uin
!!$    print*, udiff
!!$    print*, norm2(uin)-norm2(uout)
!!$    stop

  end subroutine orbit_push

  !> Computes the particle position at t = t0+0.5*dt when dt, x(t0) and u(t0) are given.
  !> 
  !> input:
  !> 
  !> real*8 dt       -- time step length [s]
  !> real*8 x(3)     -- test particle position at t = t0 [m]
  !> real*8 u(3)     -- normalized test particle momentum u=p/mc at t = t0 [1]
  !>
  !> output:
  !>
  !> real*8 xhalf(3) -- test particle position at t = t0 + 0.5*dt [m]
  !> integer err     -- error flag, negative indicates something went wrong
  !<
  subroutine orbit_halfpush(dt,x,u,xhalf,err)
    implicit none
    real*8, intent(in) :: dt
    real*8, intent(in) :: x(3)
    real*8, intent(in) :: u(3)

    real*8, intent(out) :: xhalf(3)
    integer,intent(out) :: err
     
    xhalf = x + c*dt*u/(2*sqrt(1.D0+dot_product(u,u)))

    err = 0
    if(any(isnan(xhalf))) err = ORBIT_NANRESULT

  end subroutine orbit_halfpush

  !> Prints error diagnosis if an error has occurred.
  !>
  !> input:
  !>
  !> integer err -- error flag, negative indicates something went wrong
  !<
  subroutine orbit_error(err)
    integer,intent(in) :: err
    if(err .ge. 0) then
       return
    elseif(err .eq. ORBIT_NANRESULT) then
       print*, 'Error: Result is NaN.'
    else
       print*, 'Error: Unknown error.'
    end if
  end subroutine orbit_error

end module orbit_mod
