!  -*-f90-*-  (for emacs)    vim:set filetype=fortran:  (for vim)
!
!> This module provides an adaptive time step scheme for MCCC.
!> The time steps are accepted or rejected based on the user given
!> tolerance. The tolerance indicates how much error in momentum 
!> (normalized to mc) is tolerated during one time step (the so
!> so called Error-Per-Step strategy). This module takes automatically
!> care of Wiener process generation using the Brownian Bridge when 
!> time steps are rejected.
!>
module mccca_mod
  use mccc_mod
  use wiener_mod
  implicit none

  real*8, parameter, private :: pi = 4.D0*atan(1.D0) 
  real*8, parameter, private :: eps0 = 8.854187817D-12 ![F/m]
  real*8, parameter, private :: c = 299792458.D0 ![m/s]
  real*8, parameter, private :: elemCharge = 1.60217662D-19 ![C]

  real*8, parameter, private :: MCCCA_NANRESULT        = -3
  real*8, parameter, private :: MCCCA_EXTREMELYSMALLDT = -4

  real*8, parameter, private :: MCCCA_EXTREMELYSMALLDTVAL = 1.D-12

  public :: mccca_push, mccca_evalCoefs, mccca_error

  type mccca_coefstruct 
     real*8 :: K      = 0.D0
     real*8 :: dK     = 0.D0
     real*8 :: Dpar   = 0.D0
     real*8 :: dDpar  = 0.D0
     real*8 :: Dperp  = 0.D0
     real*8 :: dDperp = 0.D0
  end type mccca_coefstruct

contains

  !> Computes the collision coefficients and stores them in a struct that is used
  !> by mccca_push(). The coefficients are calculated separately like this so that
  !> they are not unnecessarily re-calculated if the time step is rejected.
  !> 
  !> input:
  !> 
  !> mccc_special dat       -- struct that is obtained with the mccc_init() call
  !> real*8 ma              -- test particle mass [kg]
  !> real*8 qa              -- test particle charge [C]
  !> real*8 clogab(:)       -- list of coulomb logarithms for species a colliding with b [1] 
  !> real*8 mb(:)           -- list of background species masses [kg]
  !> real*8 qb(:)           -- list of background species charges [C]
  !> real*8 nb(:)           -- list of background densities [1/m^3]
  !> real*8 thb(:)          -- list of normalized background temperatures thb=T_b/(m_b*c^2) [1] 
  !> real*8 u               -- normalized test particle momentum magnitude u=|p|/mc [1]
  !>
  !> output:
  !> 
  !> mccca_coefstruct coefs -- struct containing the coefficients needed for evaluating the collisions
  !<
  subroutine mccca_evalCoefs(dat,ma,qa,clogab,mb,qb,nb,thb,u,coefs)
    implicit none
    type(mccc_special), intent(in) :: dat
    real*8, intent(in) :: ma,qa,u
    real*8, intent(in) :: clogab(:),mb(:),qb(:),nb(:),thb(:) 

    type(mccca_coefstruct), intent(out) :: coefs

    real*8 :: K,dK,Dpar,dDpar,Dperp,dDperp
    real*8 :: Kb,dKb,Dparb,dDparb,Dperpb,dDperpb

    integer :: nspecies,i

    nspecies = size(clogab)
    K=0.D0
    dK=0.D0
    Dpar=0.D0
    dDpar=0.D0
    Dperp=0.D0
    dDperp=0.D0
    do i = 1,nspecies
       call mccc_coeffs(dat,ma,qa,clogab(i),mb(i),qb(i),nb(i),thb(i),u,K=Kb,Dpar=Dparb,Dperp=Dperpb,&
            dK=dKb,dDpar=dDparb,dDperp=dDperpb)
       
       K=K+Kb
       dK=dK+dKb
       Dpar=Dpar+Dparb
       dDpar=dDpar+dDparb
       Dperp=Dperp+Dperpb
       dDperp=dDperp+dDperpb
       
    end do
    coefs%K      = K
    coefs%dK     = dK
    coefs%Dpar   = Dpar
    coefs%dDpar  = dDpar
    coefs%Dperp  = Dperp
    coefs%dDperp = dDperp

  end subroutine mccca_evalCoefs


  !> Computes the value for particle momentum after collisions with background
  !> species using Milstein method with a fixed or an adaptive time step.
  !> Returns a suggestion for the next time step. A negative suggestion indicates 
  !> the current time step was rejected, and absolute value should be used for the 
  !> next try. If no tolerance is given, Milstein method with fixed time step will
  !> be used instead. Wiener processes are generated automatically and only cleaning
  !> is required after each accepted time step.
  !>
  !> input:
  !> 
  !> mccca_coefstruct coefs -- struct containing the coefficients from mccca_evalCoefs() call
  !> real*8  wienarr(5,:)   -- array that stores the Wiener processes (Ndim = 3)
  !> real*8  uin(3)         -- normalized test particle momentum u=p/mc before collisions [1]
  !> real*8  dtin           -- candidate time step length [s]
  !> real*8  tol            -- (optional) relative tolerance. Fixed time step is used if not given
  !>
  !> output:
  !> 
  !> real*8  uout(3) -- normalized test particle momentum u=p/mc after collisions [1]
  !> real*8  dtout   -- suggestion for the next time step. dtout = dtin in case fixed step is used [s]
  !> integer err     -- error flag, negative value indicates error
  !<
  subroutine mccca_push(coefs,wienarr,uin,uout,dtin,dtout,tol,err) 
    type(mccca_coefstruct), intent(in) :: coefs

    real*8, intent(in) :: dtin 
    real*8, intent(in) :: uin(3) 
    real*8, intent(inout), allocatable :: wienarr(:,:)
    
    real*8, intent(out) :: uout(3)
    real*8, intent(out) :: dtout
    integer,intent(out) :: err
    real*8, intent(in) , optional :: tol

    real*8 :: dW(3),dW2(3) ! the change in the Wiener process during dt
    real*8 :: uhat(3) ! a unit vector parallel to pin
    real*8 :: u ! absolute value of particle momentum normalized to mc
    real*8 :: kappa_k, kappa_d(3), time, erru
    real*8 :: upara,uperp,veca(3),vecb(3)
    real*8 :: dWopt(3),dti,alpha
    integer :: i, nspecies, windex, tindex,ki,kmax
    logical :: rejected

    ! Generate a Wiener process for this step (t = time+dtin)
    time = wienarr(2,1)
    call wiener_generate(wienarr,time+dtin,windex,err)
    if(err .lt. 0) return

    dW = wienarr(3:5,windex)-wienarr(3:5,1)
    dW2 = dW**2-dtin 
    windex = 1

    ! Magnitude and unit vector of p
    u = norm2(uin)
    uhat=uin/u

    ! If no tolerance is given, we use Milstein method without adaptive time step and return
    if(.not. present(tol)) then
       upara = coefs%K*dtin+sqrt(2*coefs%Dpar)*dW(1)+coefs%dDpar*dW2(1)/2

       if( abs(dot_product(uhat,(/1.D0, 0.D0, 0.D0/))) .lt. 1.D0) then
          veca(1) = 0.D0
          veca(2) = -uhat(3)
          veca(3) = uhat(2)
          veca = veca/norm2(veca)

          vecb(1) =  veca(2)*uhat(3)-veca(3)*uhat(2)
          vecb(2) =  veca(3)*uhat(1)
          vecb(3) = -veca(2)*uhat(1)
          vecb = vecb/norm2(vecb)
       else
          veca = (/0.D0, 1.D0, 0.D0/)
          vecb = (/0.D0, 0.D0, 1.D0/)
       end if
       uout = uin+upara*uhat + sqrt(2*coefs%Dperp)*(dW(2)*veca+dW(3)*vecb)

       if(any(isnan(uout))) err = MCCCA_NANRESULT
       dtout = dtin
       return
    end if

    ! Error estimates for drift and diffusion limits
    erru = tol*(abs(coefs%K*dtin)+sqrt(2*coefs%Dpar)*sqrt(dtin))
    kappa_k = (1.D0/(2*erru))*abs(coefs%dK*coefs%K)*dtin**2
    kappa_d(1) = (1.D0/(6*erru))*abs((dW(1))**3*coefs%dDpar**2/sqrt(2*coefs%Dpar))
    kappa_d(2) = 0.D0
    kappa_d(3) = 0.D0

    ! If the time step is accepted, use Milstein method to calculate new momentum
    rejected = .true.
    if ((kappa_k .lt. 1) .and. all(kappa_d .lt. 1)) then
       upara = coefs%K*dtin+sqrt(2*coefs%Dpar)*dW(1)+coefs%dDpar*dW2(1)/2

       if( dot_product(uhat,(/1.D0, 0.D0, 0.D0/)) .lt. 1.D0) then
          veca(1) = 0.D0
          veca(2) = -uhat(3)
          veca(3) = uhat(2)
          veca = veca/norm2(veca)

          vecb(1) =  veca(2)*uhat(3)-veca(3)*uhat(2)
          vecb(2) =  veca(3)*uhat(1)
          vecb(3) = -veca(2)*uhat(1)
          vecb = vecb/norm2(vecb)
       else
          veca = (/0.D0, 1.D0, 0.D0/)
          vecb = (/0.D0, 0.D0, 1.D0/)
       end if

       uout = uin+upara*uhat + sqrt(2*coefs%Dperp)*(dW(2)*veca+dW(3)*vecb)
       time = time + dtin
       call wiener_generate(wienarr, time, windex, err)
       if(err .lt. 0) return
       rejected = .false.
    end if

    ! Different time step estimates are used depending which error estimate dominates
    ! This scheme automatically takes care of time step reduction (increase) when time step is rejected (accepted)
    dWopt = 0.9*abs(dW)*kappa_d**(-1.D0/3)
    alpha = maxval(abs(dW))/sqrt(dtin)
    if(kappa_k .gt. maxval(kappa_d)) then
       dti = min(1.5*dtin,0.8*dtin/sqrt(kappa_k))
       do ki=1,3
          call wiener_generate(wienarr, time+ki*dti/3, tindex, err)
          if(err .lt. 0) return
          dW = (abs(wienarr(3:5,windex)-wienarr(3:5,tindex)))
          if(any(dW .gt. dWopt)) then
             exit
          end if
       end do

       dtout = max(min(ki-1,3),1)*(dti/3)
    else
       kmax = 6
       if (rejected) then
          kmax = 2
       elseif (alpha .gt. 2) then
          kmax = 4
       end if

       do ki=1,kmax
          call wiener_generate(wienarr, time+ki*dtin/3, tindex, err)
          if(err .lt. 0) return
          dW = (abs(wienarr(3:5,windex)-wienarr(3:5,tindex)))
          if(any(dW .gt. dWopt)) then
             exit
          end if
       end do
       
       
       dtout = max(min(ki-1,kmax),1)*(dtin/3)
    end if
    ! Rejected step is indicated by a negative time step suggestion
    if(rejected) then
       dtout = -dtout
    end if

    if(any(isnan(uout)) .or. isnan(dtout)) err = MCCCA_NANRESULT
    if(abs(dtout) .lt. MCCCA_EXTREMELYSMALLDTVAL) err = MCCCA_EXTREMELYSMALLDT

  end subroutine mccca_push

  !> Prints error diagnosis if an error has occurred.
  !>
  !> input:
  !>
  !> integer err -- error flag, negative indicates something went wrong
  !<
  subroutine mccca_error(err)
    implicit none
    integer, intent(in) :: err

    if(err .ge. 0) then
       return
    elseif(err .eq. MCCCA_NANRESULT) then
       print*, 'Error: Result is NaN.'
    elseif(err .eq. MCCCA_EXTREMELYSMALLDT) then
       print*, 'Error: Extremely small timestep. Below ', MCCCA_EXTREMELYSMALLDTVAL, ' s'
    else
       call wiener_error(err)
    end if

  end subroutine mccca_error

end module mccca_mod
