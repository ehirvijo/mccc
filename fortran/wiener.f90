!  -*-f90-*-  (for emacs)    vim:set filetype=fortran:  (for vim)
!
!> @author
!< Konsta Sarkimaki
!<
!< A module for generating Wiener processes. When adaptive time 
!< step is used (and steps are rejected), Wiener processes are
!< generated using the so-called Brownian bridge. This module
!< contains associated helper routines as well.
!<
module wiener_mod

  implicit none

  real*8, private, parameter :: EMPTY = -999.D0 ! Index for slots not containing a Wiener process
  real*8, private, parameter :: twopi = 6.283185D0 ! 2*pi

  integer, private, parameter :: WIENER_EXCEEDEDCAPACITY    = -1
  integer, private, parameter :: WIENER_NOASSOCIATEDPROCESS = -2

  public :: wiener_generate, wiener_allocate, wiener_deallocate, wiener_clean, wiener_boxmuller

contains

  !> Allocates an array that will be used to store generated Wiener processes until
  !> they are no longer needed. Deallocation is done with subroutine wiener_deallocate.
  !> Array has format (Ndim+2) X Nslots where the first row indicates the position of 
  !> the next process in time or its own position if no such process exists. The first
  !> process is always located at the firs column. The second row stores the time 
  !> instants processes correspond. The rest of the rows stores values of the Ndim
  !> dimensional Wiener processes. Nslots value should not be overly large.
  !>
  !> input:
  !>
  !> integer Ndim         -- Number of Wiener process dimensions
  !> integer Nslots       -- Maximum number of time instances for which processes are stored.
  !> real*8  initime      -- Time (>0) at the beginning of the simulation [s]
  !>
  !> output:
  !> 
  !> real*8  wienarr(:,:) -- array that stores the Wiener processes
  !<
  subroutine wiener_allocate(wienarr, Ndim, Nslots, initime)
    implicit none
    real*8, allocatable, intent(inout) :: wienarr(:,:)
    integer, intent(in) :: Ndim
    integer, intent(in) :: Nslots
    real*8,  intent(in) :: initime

    integer :: i

    allocate (wienarr(Ndim+2,Nslots))

    ! Initialize position instances indicating all slots are empty
    i = 1
    do while(i <= Nslots)
       wienarr(1,i) = EMPTY
       i = i+1
    end do

    ! W(t_0) = 0 by the definition of the Wiener process. Here we initialize it.
    wienarr(1,1) = 1.D0
    wienarr(2,1) = initime
    i = 3
    do while(i <= Ndim)
       wienarr(i,1) = 0.D0
       i = i+1
    end do

  end subroutine wiener_allocate


  !> Deallocates the array storing the Wiener processes.
  !>
  !> input:
  !>
  !> real*8  wienarr(:,:) -- array that stores the Wiener processes
  !<
  subroutine wiener_deallocate(wienarr)
    implicit none
    real*8, allocatable, intent(inout) :: wienarr(:,:)

    deallocate(wienarr)

  end subroutine wiener_deallocate


  !> Generates a new Wiener process at a given time instant taking the Brownian bridge
  !> into account if needed.
  !>
  !> input:
  !>
  !> real*8  t            -- time for which the new process will be generated
  !> integer windex       -- index of the generated Wiener process in the Wiener array
  !>
  !> output:
  !> 
  !> real*8  wienarr(:,:) -- array that stores the Wiener processes
  !> integer err          -- error flag, negative indicates something went wrong
  !<
  subroutine wiener_generate(wienarr, t, windex,err)
    implicit none
    real*8, allocatable, intent(inout) :: wienarr(:,:)
    real*8, intent(in) :: t
    integer, intent(out) :: windex
    integer, intent(out) :: err

    integer :: Nslots, Ndim, idx, eidx, i ! Helper variables
    integer :: im, ip ! Indexes of the Wiener processes for which tm < t < tp

    windex = -1

    Nslots = size(wienarr,2)
    Ndim = size(wienarr,1)-2
    ip = -1 ! There isn't necessarily a Wiener process tp > t


    ! Find im and ip
    idx = 1
    do i = 1,Nslots
       if(wienarr(2,idx) < t) then
          im = idx
       elseif(wienarr(2,idx) == t) then
          ! It seems that the process we are looking for already exists!
          windex = idx
          return
       elseif(wienarr(2,idx) > t) then
          ip = idx
          exit
       end if

       if(wienarr(1,idx) .eq. idx) then
          ! Reached last process
          exit
       end if

       idx = idnint(wienarr(1,idx))
    end do

    ! Find an empty slot for the next process
    eidx = 0
    do i = 1,Nslots
       if( .not. (wienarr(1,i) > EMPTY)) then
          eidx = i
          exit
       end if
    end do
    if(eidx .eq. 0) then
       ! It seems that we have exceeded capacity of the Wiener array
       ! Produce an error.
       err = WIENER_EXCEEDEDCAPACITY
       return
    end if

    
    ! The eidx entry in the wiener array is always empty. We use that for temporary storage
    ! to spare one allocation/deallocation cycle.
    call wiener_boxmuller( wienarr(3:Ndim+2,eidx))

    if(ip == -1) then
       ! There are no Wiener processes existing for tp > t.
       ! The generated Wiener process then has a mean W(tm) and variance t-tm.

       wienarr(1,eidx) = 1.D0*eidx
       wienarr(2,eidx) = t
       wienarr(3:Ndim+2,eidx) = wienarr(3:Ndim+2,im) + sqrt(t-wienarr(2,im))*wienarr(3:Ndim+2,eidx)
       windex = eidx
       wienarr(1,im) = 1.D0*eidx
    else
       ! A Wiener process for tp > t exist. Generate a new process using the rules
       ! set by the Brownian bridge. The rules are:
       !
       ! mean = W(tm) + ( W(ip)-W(im) )*(t-tm)/(tp-tm)
       ! variance = (t-tm)*(tp-t)/(tp-tm)
       wienarr(2,eidx) = t
       wienarr(3:Ndim+2,eidx) = wienarr(3:Ndim+2,im) + ( wienarr(3:Ndim+2,ip)-wienarr(3:Ndim+2,im) ) &
            *( t-wienarr(2,im) )/( wienarr(2,ip)-wienarr(2,im) ) &
            + sqrt( ( t-wienarr(2,im) )*( wienarr(2,ip)-t )/( wienarr(2,ip)-wienarr(2,im) ) ) &
            *wienarr(3:Ndim+2,eidx)

       ! Sort new wiener process to its correct place
       wienarr(1,eidx) = 1.D0*ip
       wienarr(1,im) = 1.D0*eidx
       windex = eidx
    end if
  end subroutine wiener_generate

  !> Removes Wiener processes from the array that are no longer required. Processes W(t')  
  !> are redundant if t' <  t, where t is the current simulation time. Note that W(t) 
  !> should exist before W(t') are removed. This routine should be called each time when 
  !> simulation time is advanced.
  !>
  !> input:
  !>
  !> real*8  t            -- time for which the new process will be generated
  !>
  !> output:
  !> 
  !> real*8  wienarr(:,:) -- array that stores the Wiener processes
  !> integer err          -- error flag, negative indicates something went wrong
  !<
  subroutine wiener_clean(wienarr,t,err)
    implicit none
    real*8, allocatable, intent(inout) :: wienarr(:,:)
    real*8, intent(in) :: t
    integer, intent(out) :: err

    integer :: Nslots, idx, nidx, i

    err = 0

    Nslots = size(wienarr,2)

    idx = idnint(wienarr(1,1))
    do i=1,Nslots
       nidx = idnint(wienarr(1,idx))
       if(wienarr(2,idx) .lt. t) then
          wienarr(1,idx) = EMPTY
          if((idx .eq. nidx) .or. (wienarr(2,nidx) .gt. t)) then
             ! The value t does not have an associated Wiener process
             ! Produce an error
             err = WIENER_NOASSOCIATEDPROCESS
             return
          end if
          idx = nidx
          cycle
       end if
       
       ! Move t = time process as the first one
       wienarr(1,idx) = EMPTY
       wienarr(:,1) = wienarr(:,idx)
       wienarr(1,1) = 1.D0*nidx
       if(idx .eq. nidx) then
          wienarr(1,1) = 1.D0
       end if
       exit
    end do

  end subroutine wiener_clean

  !> Generates standard normally distributed random numbers using the geometric Box-Muller
  !> method.
  !>
  !> output:
  !>
  !> real*8 randVar(:) -- array that is to be populated with random numbers
  !<
  subroutine wiener_boxmuller(randVar)
    implicit none
    real*8, intent(out) :: randVar(:)

    real*8 :: x1, x2, w
    logical :: isEven   ! Indicates if even number of random numbers are requested
    integer :: i, Ndim ! Helper variables

    Ndim = size(randVar)
    isEven = mod(Ndim,2) .eq. 0
 
    do i = 1,Ndim,2
       w = 2.D0
       do while ( w >= 1.D0 )
          call random_number(x1)
          call random_number(x2)
          x1 = 2.D0*x1 - 1.D0
          x2 = 2.D0*x2 - 1.D0
          w = x1*x1 + x2*x2
       end do

       w = sqrt( (-2.D0 * log( w ) ) / w )
       randVar(i) = x1 * w
       if((i .lt. Ndim-1) .or. (isEven)) then
          randVar(i+1) = x2 * w
       end if
    end do

!!$    ! The common form which could be faster on some platforms
!!$    do i = 1,Ndim,2
!!$       call random_number(x1)
!!$       call random_number(x2)
!!$       w = sqrt(-2*log(x1))
!!$       s = cos(twopi*x2)
!!$       randVar(i) = w*s
!!$       if((i .lt. Ndim-1) .or. (isEven)) then
!!$          if(x2 .lt. 0.5D0) then
!!$             randVar(i+1) = w*sqrt(1.D0-s**2)
!!$          else
!!$             randVar(i+1) = -w*sqrt(1.D0-s**2)
!!$          end if
!!$       end if
!!$    end do
  end subroutine wiener_boxmuller

  !> Prints error description if err < 0.
  !>
  !> input:
  !>
  !> integer err -- error flag
  !<
  subroutine wiener_error(err)
    implicit none
    integer, intent(in) :: err

    if(err .ge. 0) then
       return
    else if(err .eq. WIENER_EXCEEDEDCAPACITY) then
       print*, 'Error: Number of slots in Wiener array exceeded.'
    else if(err .eq. WIENER_NOASSOCIATEDPROCESS) then
       print*, 'Error: No associated process found.'
    else
       print*, 'Error: Unknown error'
    end if

  end subroutine wiener_error



  !> Tests functionality of this module. The results are printed directly on to the screen.
  !<
  subroutine wiener_test
    implicit none

    real*8, allocatable :: wienarr(:,:), temparr(:,:)
    integer :: Ndim = 3, Nslots = 5, windex, lastindex
    integer :: Niter = 10000, iter
    integer :: err
    real*8 :: W(3,10000)
    real*8 :: initime = 0.D0
    real*8 :: meanCom(3), meanThr(3), varCom(3), varThr(3)

    print*,''
    print*,'This test program tests functionality of wiener module.'
    print*,'The results are printed directly on to the screen.'
    print*,''

    write(*,*) 'Allocating a 5 slot array of 3D Wiener processes...'
    call wiener_allocate(wienarr, Ndim, Nslots, initime)
    write(*,*) 'Done. The index and time values are:'
    write(*,*) ''
    write(*,*) wienarr(1,:)
    write(*,*) wienarr(2,:)
    write(*,*) wienarr(3,:)
    write(*,*) ''
    write(*,*) 'Expected: [1 -999 -999 -999 -999]'
    write(*,*) ''
    write(*,*) ''

    write(*,*) 'Generating a new Wiener process for t = 1.0.'
    call wiener_generate(wienarr, 1.D0, windex,err)
    call wiener_error(err)
    write(*,*) 'The index and time values are:'
    write(*,*) ''
    write(*,*) wienarr(1,:)
    write(*,*) wienarr(2,:)
    write(*,*) wienarr(3,:)
    write(*,*) ''
    write(*,*) 'Expected: [2 2 -999 -999 -999]'
    write(*,*) ''
    write(*,*) ''

    write(*,*) 'Generating a new Wiener process for t = 0.5.'
    call wiener_generate(wienarr, 0.5D0, windex,err)
    call wiener_error(err)
    write(*,*) 'The index and time values are:'
    write(*,*) ''
    write(*,*) wienarr(1,:)
    write(*,*) wienarr(2,:)
    write(*,*) wienarr(3,:)
    write(*,*) ''
    write(*,*) 'Expected: [3 2 2 -999 -999 -999 -999]'
    write(*,*) ''
    write(*,*) ''

    write(*,*) 'Generating a new Wiener process for t = 0.75.'
    call wiener_generate(wienarr, 0.75D0, windex,err)
    call wiener_error(err)
    write(*,*) 'The index and time values are:'
    write(*,*) ''
    write(*,*) wienarr(1,:)
    write(*,*) wienarr(2,:)
    write(*,*) wienarr(3,:)
    write(*,*) ''
    write(*,*) 'Expected: [3 2 4 2 -999]'
    write(*,*) ''
    write(*,*) ''

    write(*,*) 'Generating a new Wiener process for t = 2.'
    call wiener_generate(wienarr, 2.D0, windex,err)
    call wiener_error(err)
    write(*,*) 'The index and time values are:'
    write(*,*) ''
    write(*,*) wienarr(1,:)
    write(*,*) wienarr(2,:)
    write(*,*) wienarr(3,:)
    write(*,*) ''
    write(*,*) 'Expected: [3 5 4 2 5]'
    write(*,*) ''
    write(*,*) ''

    write(*,*) 'Next we try to generate value at t = 1.5.'
    call wiener_generate(wienarr, 1.5D0, windex,err)
    call wiener_error(err)
    write(*,*) 'This should have resulted in error.'
    write(*,*) ''
    write(*,*) ''

    write(*,*) 'Assuming simulation time is 1.0, clean the array.'
    call wiener_clean(wienarr,1.D0,err)
    call wiener_error(err)
    write(*,*) 'The index and time values are:'
    write(*,*) ''
    write(*,*) wienarr(1,:)
    write(*,*) wienarr(2,:)
    write(*,*) wienarr(3,:)
    write(*,*) ''
    write(*,*) 'Expected: [5 -999 -999 -999 5]'
    write(*,*) ''
    write(*,*) ''

    write(*,*) 'Finally, check that Wiener processes are indeed generated correctly.'
    write(*,*) 'With simulation time 1.0, mean and var for W(3.0) calculated N = 1000'
    write(*,*) 'iterations is (expected values are in parenthesis)'

    allocate(temparr(Ndim+2,Nslots))
    do iter = 1,Niter
       temparr(:,:) = wienarr(:,:)
       call wiener_generate(temparr, 3.D0, windex,err)
       call wiener_error(err)
       W(:,iter) = temparr(3:Ndim+2,windex)
    end do

    call wiener_generate(temparr, 2.D0, lastindex,err)
    call wiener_error(err)
    meanCom = sum(W,2)/Niter
    meanThr = wienarr(3:Ndim+2,lastindex)
    varCom(1) = sum((W(1,:)-meanCom(1))**2,1)/(Niter-1)
    varCom(2) = sum((W(2,:)-meanCom(2))**2,1)/(Niter-1)
    varCom(3) = sum((W(3,:)-meanCom(3))**2,1)/(Niter-1)
    varThr(:) = (3.D0-2.D0)

    write(*,*) '- When W(t>3.0) does not exist:'
    write(*,*) ''
    write(*,*) meanCom(1),varCom(1),'(',meanThr(1), varThr(1),')'
    write(*,*) meanCom(2),varCom(2),'(',meanThr(2), varThr(2),')'
    write(*,*) meanCom(3),varCom(3),'(',meanThr(3), varThr(3),')'
    write(*,*) ''

    call wiener_generate(wienarr, 3.1D0, windex,err)
    call wiener_error(err)
    do iter = 1,Niter
       temparr(:,:) = wienarr(:,:)
       call wiener_generate(temparr, 3.0D0, windex,err)
       call wiener_error(err)
       W(:,iter) = temparr(3:Ndim+2,windex)
    end do
    ! Analytical values for Brownian bridge:
    ! mean = W(tm) + ( W(ip)-W(im) )*(t-tm)/(tp-tm)
    ! variance = (t-tm)*(tp-t)/(tp-tm)
    call wiener_generate(temparr, 2.D0, lastindex,err)
    call wiener_error(err)
    call wiener_generate(wienarr, 3.1D0, windex,err)
    call wiener_error(err)
    meanCom = sum(W,2)/Niter
    meanThr = wienarr(3:Ndim+2,lastindex) + (wienarr(3:Ndim+2,windex) &
         - wienarr(3:Ndim+2,lastindex))*(3.D0-2.D0)/(3.1D0-2.D0)

    varCom(1) = sum((W(1,:)-meanCom(1))**2,1)/(Niter-1)
    varCom(2) = sum((W(2,:)-meanCom(2))**2,1)/(Niter-1)
    varCom(3) = sum((W(3,:)-meanCom(3))**2,1)/(Niter-1)

    varThr(:) = (3.D0-2.D0)*(3.1D0-3.D0)/(3.1D0-2.D0)

    write(*,*) '- When W(3.1) exists:'
    write(*,*) ''
    write(*,*) meanCom(1),varCom(1),'(',meanThr(1), varThr(1),')'
    write(*,*) meanCom(2),varCom(2),'(',meanThr(2), varThr(2),')'
    write(*,*) meanCom(3),varCom(3),'(',meanThr(3), varThr(3),')'
    write(*,*) ''

    call wiener_deallocate(wienarr)
    deallocate(temparr)

    write(*,*) 'Test complete.'
    write(*,*) ''

  end subroutine wiener_test

end module wiener_mod
