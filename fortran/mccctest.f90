!  -*-f90-*-  (for emacs)    vim:set filetype=fortran:  (for vim)
!
!> This module contains test programs where test particle
!> experiences collisions with the background plasma. Tests for
!> slowing down and thermal particle are implemented. Uses integration
!> schemes from mccc, mccca, and mcccgc modules. The test programs
!> also illustrate on how MCCC package can be used.
!<
module mccctest_mod

  use wiener_mod
  use mccc_mod
  use mccca_mod
  use mcccgc_mod
  use orbit_mod
  implicit none

  private
  real*8, parameter :: elemCharge = 1.60217662D-19 ![C]
  real*8, parameter :: c = 299792458.D0 ![m/s]

  real*8, parameter :: uminxp = -1.D0
  real*8, parameter :: umaxxp = 3.D0
  real*8, parameter :: thminxp = -5.D0
  real*8, parameter :: thmaxxp = -1.D0 
  integer, parameter :: nu = 201
  integer, parameter :: nth = 201

  public :: mccctest_MJ, mccctest_SD

  integer, parameter :: mccctest_PARTICLE_EULER         = 10
  integer, parameter :: mccctest_PARTICLE_MILSTEIN      = 11
  integer, parameter :: mccctest_GUIDINGCENTER_FIXED    = 20
  integer, parameter :: mccctest_GUIDINGCENTER_ADAPTIVE = 21
  
contains

  subroutine mccctest_MJ(integrator)
    implicit none
    integer, intent(in) :: integrator

    ! For storing collision coefficients
    type(mcccgc_coefstruct) :: gccoefs
    type(mccca_coefstruct)  :: prtcoefs

    ! Background 
    integer, parameter :: nspec=1
    type(mccc_special) :: dat   
    real*8 :: clogab(nspec) ! [1]
    real*8 :: mb(nspec) ! [kg]
    real*8 :: qb(nspec) ! [C]
    real*8 :: nb(nspec) ! [1/m^3]
    real*8 :: thb(nspec) ! [1]
    real*8 :: B(3)
    real*8 :: E(3) = [0.D0, 0.D0, 0.D0]

    ! Particle
    real*8 :: uprt(3)
    real*8 :: uin, uout ! [p/mc]
    real*8 :: uvecin(3), uvecout(3), uorb(3) ! [p/mc]
    real*8 :: xorbin(3), xorbout(3), xorbhalf(3), xorb(3) ! [p/mc] 
    real*8 :: xiin, xiout
    real*8 :: dx(3)
    real*8 :: ma ! [kg]
    real*8 :: qa ! [C}

    ! Simulation specific
    real*8 :: dtin, dtout, dt0, time, tmax ! [s]
    real*8 :: cpuTIMEi, cpuTIMEf ! [s]
    real*8 :: tol(2)
    real*8 :: abstol ! [p/mc]
    real*8, allocatable :: wienarr(:,:)
    real*8  :: rnd(3)
    real*8  :: cutoff
    integer :: Nsteps, Nrejected
    integer :: n,i,Nprt
    integer :: err
    
    ! Histograms
    integer :: ntBins
    integer :: nxiBins
    integer :: nuBins
    integer :: nxBins
    integer :: tidx
    integer :: uidx
    integer :: xiidx
    integer :: xidx
    real*8 :: tHistMax
    real*8 :: tHistBinWidth
    real*8 :: uHistMax
    real*8 :: uHistBinWidth
    real*8 :: xHistMax
    real*8 :: xHistBinWidth
    real*8 :: xiHistBinWidth
    real*8,allocatable :: uHist(:,:)
    real*8,allocatable :: xiHist(:,:)
    real*8,allocatable :: xHist(:,:)

    ! For input output
    logical :: storage_file_on_disk
    integer, parameter :: chn=989
    character(20), parameter :: storage_file='mccc.data'

    print*,''
    print*,'This test program evaluates a thermal test particle for several collision times'
    print*,'while gathering its pitch and momentum distributions. Different integration schemes can be used.'
    print*,'The pitch distribution will be printed in unittest_MJ_*_xi.out, momentum'
    print*,'distribution in unittest_MJ_*_u.out and spatial in unittest_MJ_*_x.out.'
    print*,''


    ! Define the background distribution to be electrons
    ! at temperature T/mc^2 = 0.1
    thb = 1.D-1 ! Background temperature
    B = [0.D0, 1.D0, 0.D0] ! Background magnetic field
    E = [0.D0, 0.D0, 0.D0]
    mb = 9.10938356D-31
    qb = -elemCharge
    nb = 1.D20
    print*,''
    print*,'Background:'
    print*, 
    print*,'Species theta'         ,thb
    print*,'Species mass [kg]'     ,mb
    print*,'Species charge [C]'    ,qb
    print*,'Species density [m^-3]',nb

    ! Define the test particle to be electron
    ! and set the kinetic energy equal to the
    ! background temperature   
    ma = 9.10938356D-31
    qa = -elemCharge
    !uprt = [0.D0,-sqrt((1.D0+3.D0*thb(1))**2-1.D0),0.D0]
    uprt = [0.D0,-sqrt((1.D0+1.D0*thb(1))**2-1.D0),0.D0]
    print*,''
    print*,'Test particle:'
    print*,''
    print*,'Mass [kg]'                , ma
    print*,'Charge [C]'               , qa
    print*,'Momentum normalized to mc', norm2(uprt)

    ! Simulation parameters: simulation time and tolerance
    tmax = 2.D-2
    tol  = [1.D-2,1.D-2]
    dtin = 1.0D-6
    Nprt = 1000
    cutoff = 2.D-1*maxval(thb)
    print*,''
    print*,'Simulation parameters:'
    print*,''
    print*,'Maximum time'       , tmax
    print*,'Tolerance'          , tol
    print*,'(Initial) time step', dtin
    print*,'Number of particles', Nprt

    ! Initialize the histograms and set the phist max value
    ntbins = 100
    tHistBinWidth=tmax/ntBins
    tHistMax = tmax

    nxBins = 50
    nxiBins = 100
    xiHistBinWidth=2.D0/nxiBins
    nuBins  = 500
    uHistMax=sqrt((1.D0+thb(1))**2-1.D0)*25
    uHistBinWidth=uHistMax/nuBins
    xHistMax=2.D-2
    xHistBinWidth=xHistMax/nxBins
    allocate(uHist(nuBins,ntbins),xiHist(nxiBins,ntbins),xHist(nxBins,ntbins))
    uHist=0.D0
    xiHist=0.D0
    xHist=0.D0


    ! Initialize the look-up tables
    print*,''
    write(*,*) 'Initializing look-up tables...'
    inquire(file=storage_file,exist=storage_file_on_disk)
    if(storage_file_on_disk) then
       dat = mccc_readdat(storage_file)
    else
       dat=mccc_init(uminxp,umaxxp,thminxp,thmaxxp,nu,nth)
       err = mccc_writedat(dat,storage_file)
    end if
    print*,'Done!'

    ! Simulate and accumulate the histograms
    print*,''
    write(*,*) 'Begin simulation...'    

    Nsteps = 0
    Nrejected = 0

    call CPU_TIME(cpuTIMEi)

    markerloop: do n=1,Nprt
       ! Init particle 
       time = 0.D0
       xorbin = 0.D0
       uin = norm2(uprt)
       select case(integrator)
       case(mccctest_PARTICLE_EULER, mccctest_PARTICLE_MILSTEIN)
          uvecin = uprt
          call wiener_allocate(wienarr,3, 40,time)
       case(mccctest_GUIDINGCENTER_FIXED, mccctest_GUIDINGCENTER_ADAPTIVE)
          xiin = dot_product(B,uprt)/(uin*B(2))
          call wiener_allocate(wienarr,5, 30,time)
       case default
          print*, 'Unknown integrator. The possible choices are:'
          print*, 'PARTICLE EULER:'         ,mccctest_PARTICLE_EULER
          print*, 'PARTICLE MILSTEIN:'      ,mccctest_PARTICLE_MILSTEIN
          print*, 'GUIDINGCENTER FIXED:'    ,mccctest_GUIDINGCENTER_FIXED
          print*, 'GUIDINGCENTER ADAPTIVE:' ,mccctest_GUIDINGCENTER_ADAPTIVE
          stop
       end select

       timeloop: do while(time .lt. tmax)
          ! Take simulation time step, begin by evaluating collision coefficients
          dtout = -1.D0
          call mccc_clog(ma,qa,mb,qb,nb,thb,uin,clogab)
          select case(integrator)
          case(mccctest_PARTICLE_MILSTEIN)
             call mccca_evalCoefs(dat,ma,qa,clogab,mb,qb,nb,thb,norm2(uvecin),prtcoefs)
          case(mccctest_GUIDINGCENTER_FIXED,mccctest_GUIDINGCENTER_ADAPTIVE)
             call mcccgc_evalCoefs(dat,ma,qa,clogab,mb,qb,nb,thb,uin,xiin,gccoefs)
          end select

          ! Iterate until step is accepted
          timesteploop: do while(dtout .lt. 0)
             select case(integrator)
             case(mccctest_PARTICLE_EULER)
                call wiener_boxmuller(rnd)
                call mccc_push(dat,ma,qa,clogab,mb,qb,nb,thb,dtin,rnd,uvecin,uvecout,err)
                call mccc_error(err)

                uorb = uvecout
                call orbit_halfpush(dtin,xorbin,uorb,xorbhalf,err)
                call orbit_error(err)
                call orbit_push(ma,qa,E,B,dtin,xorbhalf,xorbout,uorb,uvecout,err)
                call orbit_error(err)
                if(err .lt. 0) then
                   call wiener_deallocate(wienarr)
                   cycle markerloop
                end if
 
                dx = xorbout(3)+(sqrt(uvecout(1)**2+uvecout(3)**2)*ma*c/(qa*norm2(B)))*&
                     uvecout(1)/norm2(uvecout)

                dtout = dtin
             case(mccctest_PARTICLE_MILSTEIN)
                call mccca_push(prtcoefs,wienarr,uvecin,uvecout,dtin,dtout,tol(1),err=err)
                call mccca_error(err)

                if(dtout .gt. 0) then
                   uorb = uvecout
                   call orbit_halfpush(dtin,xorbin,uorb,xorbhalf,err)
                   call orbit_error(err)
                   call orbit_push(ma,qa,E,B,dtin,xorbhalf,xorbout,uorb,uvecout,err)
                   call orbit_error(err)
                   if(err .lt. 0) then
                      call wiener_deallocate(wienarr)
                      cycle markerloop
                   end if
                   
                   dx = xorbout(3)+(sqrt(uvecout(1)**2+uvecout(3)**2)*ma*c/(qa*norm2(B)))*&
                        uvecout(1)/norm2(uvecout)
                end if

                !dtout = dtin             
             case(mccctest_GUIDINGCENTER_FIXED)
                call mcccgc_push(gccoefs,wienarr,B,cutoff,uin,uout,xiin,xiout,dx,dtin,dtout,err=err)
                call mcccgc_error(err)
             case(mccctest_GUIDINGCENTER_ADAPTIVE)
                call mcccgc_push(gccoefs,wienarr,B,cutoff,uin,uout,xiin,xiout,dx,dtin,dtout,tol,err=err)
                call mcccgc_error(err)
                xorbout = xorbin+dx
                dx = xorbout
             end select

             if(err .lt. 0) then
                call wiener_deallocate(wienarr)
                cycle markerloop
             end if

             if(dtout .gt. 0) then
                time = time+dtin
                dt0 = dtin
                Nsteps = Nsteps+1

                if((integrator .eq. mccctest_PARTICLE_MILSTEIN) .or. &
                     (integrator .eq. mccctest_GUIDINGCENTER_ADAPTIVE)) then
                   call wiener_clean(wienarr,time,err)
                   if(err .lt. 0) then
                      call wiener_error(err)
                      call wiener_deallocate(wienarr)
                      cycle markerloop
                   end if
                end if
             else
                Nrejected = Nrejected+1
             end if
             dtin = abs(dtout)
          end do timesteploop

          ! Update histograms
          tidx=findex(time-dt0/2,tHistBinWidth,ntbins)
          select case(integrator)
          case(mccctest_PARTICLE_EULER, mccctest_PARTICLE_MILSTEIN)
             uidx=findex((norm2(uvecout)+norm2(uvecin))/2,uhistBinWidth,nubins)
             xiidx=findex(&
                  (dot_product(B,uvecin)/(norm2(B)*norm2(uvecin))+dot_product(B,uvecout)/(norm2(B)*norm2(uvecout)))/2+1.D0,&
                  xihistBinWidth,nxibins)
             xidx=findex(dx(3)+xHistMax/2,xhistBinWidth,nxbins)

             uHist(uidx,tidx)=uHist(uidx,tidx)+dt0
             xiHist(xiidx,tidx)=xiHist(xiidx,tidx)+dt0
             !xHist(xidx,tidx)=xHist(xidx,tidx)+dt0

             xorbin=xorbout
             uvecin=uvecout 
          case(mccctest_GUIDINGCENTER_FIXED, mccctest_GUIDINGCENTER_ADAPTIVE)
             uidx=findex((uout+uin)/2,uhistBinWidth,nubins)
             xiidx=findex((xiout+xiin)/2+1.D0,xihistBinWidth,nxibins)
             xidx=findex(xorbout(3)+xHistMax/2,xhistBinWidth,nxbins)

             uHist(uidx,tidx)=uHist(uidx,tidx)+dt0
             xiHist(xiidx,tidx)=xiHist(xiidx,tidx)+dt0
             !xHist(xidx,tidx)=xHist(xidx,tidx)+dt0

             xorbin=xorbout
             uin=uout
             xiin=xiout
          end select


       end do timeloop
       xHist(xidx,tidx)=xHist(xidx,tidx)+dt0

       call wiener_deallocate(wienarr)
       !print*, xorbout(3)
    end do markerloop

    call CPU_TIME(cpuTIMEf)

    select case(integrator)
    case(mccctest_PARTICLE_EULER) 
       call writedist(uhist,0.D0,tHistBinWidth,0.D0,uHistBinWidth,'unittest_MJ_PRTF_u.out')
       call writedist(xihist,0.D0,tHistBinWidth,-1.D0,xiHistBinWidth,'unittest_MJ_PRTF_xi.out')
       call writedist(xhist,0.D0,tHistBinWidth,-xHistMax/2,xHistBinWidth,'unittest_MJ_PRTF_x.out')
    case(mccctest_PARTICLE_MILSTEIN)
       call writedist(uhist,0.D0,tHistBinWidth,0.D0,uHistBinWidth,'unittest_MJ_PRTA_u.out')
       call writedist(xihist,0.D0,tHistBinWidth,-1.D0,xiHistBinWidth,'unittest_MJ_PRTA_xi.out')
       call writedist(xhist,0.D0,tHistBinWidth,-xHistMax/2,xHistBinWidth,'unittest_MJ_PRTA_x.out')
    case(mccctest_GUIDINGCENTER_FIXED)
       call writedist(uhist,0.D0,tHistBinWidth,0.D0,uHistBinWidth,'unittest_MJ_GCF_u.out')
       call writedist(xihist,0.D0,tHistBinWidth,-1.D0,xiHistBinWidth,'unittest_MJ_GCF_xi.out')
       call writedist(xhist,0.D0,tHistBinWidth,-xHistMax/2,xHistBinWidth,'unittest_MJ_GCF_x.out')
    case(mccctest_GUIDINGCENTER_ADAPTIVE)
       call writedist(uhist,0.D0,tHistBinWidth,0.D0,uHistBinWidth,'unittest_MJ_GCA_u.out')
       call writedist(xihist,0.D0,tHistBinWidth,-1.D0,xiHistBinWidth,'unittest_MJ_GCA_xi.out')
       call writedist(xhist,0.D0,tHistBinWidth,-xHistMax/2,xHistBinWidth,'unittest_MJ_GCA_x.out')
    end select
   
    deallocate(uHist,xiHist,xHist)
    err = mccc_uninit(dat)
    
    print*,''
    print*,'Simulation complete.'
    print*,''
    print*,'Accepted time steps: ', Nsteps
    print*,'Rejected: ', Nrejected
    print*,'Fraction: ', (1.D0*Nrejected)/Nsteps
    print*,'Elapsed cpu time', cpuTIMEf-cpuTIMEi

  end subroutine mccctest_MJ


subroutine mccctest_SD(integrator)
    implicit none
    integer, intent(in) :: integrator

    ! For storing collision coefficients
    type(mcccgc_coefstruct) :: gccoefs
    type(mccca_coefstruct)  :: prtcoefs

    ! Background 
    integer, parameter :: nspec=1
    type(mccc_special) :: dat   
    real*8 :: clogab(nspec) ! [1]
    real*8 :: mb(nspec) ! [kg]
    real*8 :: qb(nspec) ! [C]
    real*8 :: nb(nspec) ! [1/m^3]
    real*8 :: thb(nspec) ! [1]
    real*8 :: B(3)

    ! Particle
    real*8 :: uprt(3)
    real*8 :: uin, uout ! [p/mc]
    real*8 :: uvecin(3), uvecout(3) ! [p/mc]
    real*8 :: xiin, xiout
    real*8 :: x(3), dx(3)
    real*8 :: ma ! [kg]
    real*8 :: qa ! [C}

    ! Simulation specific
    real*8 :: dtin, dtout, dt0, time ! [s]
    real*8 :: cpuTIMEi, cpuTIMEf ! [s]
    real*8 :: umin
    real*8 :: tol(2)
    real*8 :: abstol ! [p/mc]
    real*8, allocatable :: wienarr(:,:)
    real*8  :: rnd(3)
    real*8  :: cutoff
    real*8  :: meanSDtime
    integer :: Nsteps, Nrejected
    integer :: n, Nprt
    integer :: err
    
    ! Histograms
    integer :: ntBins
    integer :: nxiBins
    integer :: nuBins
    integer :: nxBins
    integer :: tidx
    integer :: uidx
    integer :: xiidx
    integer :: xidx
    real*8 :: tHistMax
    real*8 :: tHistBinWidth
    real*8 :: uHistMax
    real*8 :: uHistBinWidth
    real*8 :: xHistMax
    real*8 :: xHistBinWidth
    real*8 :: xiHistBinWidth
    real*8,allocatable :: uHist(:,:)
    real*8,allocatable :: xiHist(:,:)
    real*8,allocatable :: xHist(:,:)
    real*8,allocatable :: tHist(:)

    ! For input output
    logical :: storage_file_on_disk
    integer, parameter :: chn=989
    character(20), parameter :: storage_file='mccc.data'

    print*,''
    print*,'This test program evaluates a fast test particle until it thermalizes'
    print*,'while gathering its pitch and momentum distributions. Different integration schemes can be used.'
    print*,'The pitch distribution will be printed in unittest_SD_*_xi.out, momentum'
    print*,'distribution in unittest_SD_*_u.out and slowing down time in unittest_SD_*_t.out.'
    print*,''


    ! Define the background distribution to be electrons
    ! at temperature T/mc^2 = 0.1
    thb = 1.D-2 ! Background temperature
    B = [0.D0, 5.D0, 0.D0] ! Background magnetic field
    mb = 9.10938356D-31
    qb = -elemCharge
    nb = 1.D20
    print*,''
    print*,'Background:'
    print*, 
    print*,'Species theta'         ,thb
    print*,'Species mass [kg]'     ,mb
    print*,'Species charge [C]'    ,qb
    print*,'Species density [m^-3]',nb

    ! Define the test particle to be electron
    ! and set the kinetic energy equal to the
    ! background temperature   
    ma = 9.10938356D-31
    qa = -elemCharge
    uprt = [0.D0,-5.D0,0.D0]
    print*,''
    print*,'Test particle:'
    print*,''
    print*,'Mass [kg]'                , ma
    print*,'Charge [C]'               , qa
    print*,'Initial energy (eV)'      , (sqrt(norm2(uprt)+1)-1)*ma*c**2/elemCharge

    ! Simulation parameters: simulation time and tolerance
    !umin = (thb(1)+1.D0)**2-1.D0! u = (E/mc^2+1)^2-1
    umin = 1.D0
    tol  = [2.D-3,2.D-3]
    dtin = 3.D-6
    Nprt = 2000
    cutoff = 1.D-1*maxval(thb)
    print*,''
    print*,'Simulation parameters:'
    print*,''
    print*,'Energy limit (eV)'  , (sqrt(1.D0+umin**2)-1.D0)*ma*c**2/elemCharge ! E = (sqrt(1+u^2)-1)mc^2
    print*,'Tolerance'          , tol
    print*,'(Initial) time step', dtin
    print*,'Number of particles', Nprt

    !call random_seed(size=1)
    
    ! Initialize the histograms and set the phist max value
    ntbins = 400
    tHistMax = 2.D-1
    tHistBinWidth=tHistMax/ntBins

    nxBins = 200
    nxiBins = 100
    xiHistBinWidth=2.D0/nxiBins
    nuBins  = 1000
    uHistMax=sqrt((1.D0+thb(1))**2-1.D0)*50
    uHistBinWidth=uHistMax/nuBins
    xHistMax=2.D-4
    xHistBinWidth=xHistMax/nxBins
    allocate(uHist(nuBins,ntbins),xiHist(nxiBins,ntbins),xHist(nxBins,ntbins),tHist(ntBins))
    uHist=0.D0
    xiHist=0.D0
    xHist=0.D0
    tHist=0.D0


    ! Initialize the look-up tables
    print*,''
    write(*,*) 'Initializing look-up tables...'
    inquire(file=storage_file,exist=storage_file_on_disk)
    if(storage_file_on_disk) then
       dat = mccc_readdat(storage_file)
    else
       dat=mccc_init(uminxp,umaxxp,thminxp,thmaxxp,nu,nth)
       err=mccc_writedat(dat,storage_file)
    end if
    print*,'Done!'

    ! Simulate and accumulate the histograms
    print*,''
    write(*,*) 'Begin simulation...'

    
    meanSDtime = 0.D0
    Nsteps = 0
    Nrejected = 0

    call CPU_TIME(cpuTIMEi)

    markerloop: do n=1,Nprt
       ! Init particle 
       time = 0.D0
       uin = norm2(uprt)
       uvecin = uprt
       select case(integrator)
       case(mccctest_PARTICLE_EULER, mccctest_PARTICLE_MILSTEIN)    
          call wiener_allocate(wienarr,3, 16*2,time)
       case(mccctest_GUIDINGCENTER_FIXED, mccctest_GUIDINGCENTER_ADAPTIVE)
          xiin = dot_product(B,uprt)/(uin*B(2))
          x = 0.D0
          call wiener_allocate(wienarr,5, 40,time)
       case default
          print*, 'Unknown integrator. The possible choices are:'
          print*, 'PARTICLE EULER:'         ,mccctest_PARTICLE_EULER
          print*, 'PARTICLE MILSTEIN:'      ,mccctest_PARTICLE_MILSTEIN
          print*, 'GUIDINGCENTER FIXED:'    ,mccctest_GUIDINGCENTER_FIXED
          print*, 'GUIDINGCENTER ADAPTIVE:' ,mccctest_GUIDINGCENTER_ADAPTIVE
          stop
       end select

       timeloop: do while((uin .gt. umin) .and. (norm2(uvecin) .gt. umin))
          ! Take simulation time step, begin by evaluating collision coefficients
          dtout = -1.D0

          call mccc_clog(ma,qa,mb,qb,nb,thb,uin,clogab)
          select case(integrator)
          case(mccctest_PARTICLE_MILSTEIN)
             call mccca_evalCoefs(dat,ma,qa,clogab,mb,qb,nb,thb,norm2(uvecin),prtcoefs)
          case(mccctest_GUIDINGCENTER_FIXED,mccctest_GUIDINGCENTER_ADAPTIVE)
             call mcccgc_evalCoefs(dat,ma,qa,clogab,mb,qb,nb,thb,uin,xiin,gccoefs)
          end select

          ! Iterate until step is accepted
          timesteploop: do while(dtout .lt. 0)
             select case(integrator)
             case(mccctest_PARTICLE_EULER)
                call wiener_boxmuller(rnd)
                call mccc_push(dat,ma,qa,clogab,mb,qb,nb,thb,dtin,rnd,uvecin,uvecout,err)
                call mccc_error(err)
                dtout = dtin
             case(mccctest_PARTICLE_MILSTEIN)
                call mccca_push(prtcoefs,wienarr,uvecin,uvecout,dtin,dtout,tol(1),err=err)
                call mccca_error(err)
             case(mccctest_GUIDINGCENTER_FIXED)
                call mcccgc_push(gccoefs,wienarr,B,cutoff,uin,uout,xiin,xiout,dx,dtin,dtout,err=err)
                call mcccgc_error(err)
             case(mccctest_GUIDINGCENTER_ADAPTIVE)
                call mcccgc_push(gccoefs,wienarr,B,cutoff,uin,uout,xiin,xiout,dx,dtin,dtout,tol,err=err)
                call mcccgc_error(err)
             end select

             if(err .lt. 0) then
                call wiener_deallocate(wienarr)
                cycle markerloop
             end if

             if(dtout .gt. 0) then
                time = time+dtin
                dt0 = dtin
                Nsteps = Nsteps+1

                if((integrator .eq. mccctest_PARTICLE_MILSTEIN) .or. &
                     (integrator .eq. mccctest_GUIDINGCENTER_FIXED) .or. &
                     (integrator .eq. mccctest_GUIDINGCENTER_ADAPTIVE)) then
                   call wiener_clean(wienarr,time,err)
                   if(err .lt. 0) then
                      call wiener_error(err)
                      call wiener_deallocate(wienarr)
                      cycle markerloop
                   end if
                end if
             else
                Nrejected = Nrejected+1
             end if
             dtin = abs(dtout)
          end do timesteploop

          ! Update histograms
          tidx=findex(time-dt0/2,tHistBinWidth,ntbins)
          select case(integrator)
          case(mccctest_PARTICLE_EULER, mccctest_PARTICLE_MILSTEIN)
             uidx=findex(norm2(uvecout+uvecin)/2,uhistBinWidth,nubins)
             xiidx=findex(&
                  (dot_product(B,uvecin)/(norm2(B)*norm2(uvecin))+dot_product(B,uvecout)/(norm2(B)*norm2(uvecout)))/2+1.D0,&
                  xihistBinWidth,nxibins)

             uHist(uidx,tidx)=uHist(uidx,tidx)+dt0
             xiHist(xiidx,tidx)=xiHist(xiidx,tidx)+dt0

             uvecin=uvecout
          case(mccctest_GUIDINGCENTER_FIXED, mccctest_GUIDINGCENTER_ADAPTIVE)
             uidx=findex((uout+uin)/2,uhistBinWidth,nubins)
             xiidx=findex((xiout+xiin)/2+1.D0,xihistBinWidth,nxibins)
             xidx=findex(norm2(dx),xhistBinWidth,nxbins)

             uHist(uidx,tidx)=uHist(uidx,tidx)+dt0
             xiHist(xiidx,tidx)=xiHist(xiidx,tidx)+dt0
             xHist(xidx,tidx)=xHist(xidx,tidx)+dt0

             x=dx
             uin=uout
             xiin=xiout
          end select
          

       end do timeloop

       call wiener_deallocate(wienarr)

       tHist(tidx)=tHist(tidx)+1.D0
       meanSDtime = meanSDtime + time
    end do markerloop

    call CPU_TIME(cpuTIMEf)

    select case(integrator)
    case(mccctest_PARTICLE_EULER) 
       call writedist(uhist,0.D0,tHistBinWidth,0.D0,uHistBinWidth,'unittest_SD_PRTF_u.out')
       call writedist(xihist,0.D0,tHistBinWidth,-1.D0,xiHistBinWidth,'unittest_SD_PRTF_xi.out')
       call writedist1D(thist,0.D0,tHistBinWidth,'unittest_SD_PRTF_t.out')
    case(mccctest_PARTICLE_MILSTEIN)
       call writedist(uhist,0.D0,tHistBinWidth,0.D0,uHistBinWidth,'unittest_SD_PRTA_u.out')
       call writedist(xihist,0.D0,tHistBinWidth,-1.D0,xiHistBinWidth,'unittest_SD_PRTA_xi.out')
       call writedist1D(thist,0.D0,tHistBinWidth,'unittest_SD_PRTA_t.out')
    case(mccctest_GUIDINGCENTER_FIXED)
       call writedist(uhist,0.D0,tHistBinWidth,0.D0,uHistBinWidth,'unittest_SD_GCF_u.out')
       call writedist(xihist,0.D0,tHistBinWidth,-1.D0,xiHistBinWidth,'unittest_SD_GCF_xi.out')
       call writedist(xhist,0.D0,tHistBinWidth,0.D0,xHistBinWidth,'unittest_SD_GCF_x.out')
       call writedist1D(thist,0.D0,tHistBinWidth,'unittest_SD_GCF_t.out')
    case(mccctest_GUIDINGCENTER_ADAPTIVE)
       call writedist(uhist,0.D0,tHistBinWidth,0.D0,uHistBinWidth,'unittest_SD_GCA_u.out')
       call writedist(xihist,0.D0,tHistBinWidth,-1.D0,xiHistBinWidth,'unittest_SD_GCA_xi.out')
       call writedist(xhist,0.D0,tHistBinWidth,0.D0,xHistBinWidth,'unittest_SD_GCA_x.out')
       call writedist1D(thist,0.D0,tHistBinWidth,'unittest_SD_GCA_t.out')
    end select

    
   
    deallocate(uHist,xiHist,xHist,tHist)
    err = mccc_uninit(dat)
    
    print*,''
    print*,'Simulation complete.'
    print*,''
    print*,'Accepted time steps: ', Nsteps
    print*,'Rejected: ', Nrejected
    print*,'Fraction: ', (1.D0*Nrejected)/Nsteps
    print*,'Elapsed cpu time', cpuTIMEf-cpuTIMEi
    print*,'Mean slowing down time', meanSDtime/Nprt

  end subroutine mccctest_SD

  integer function findex(val,histBinWidth,nBins) 
    real*8, intent(in) :: val
    real*8, intent(in) :: histBinWidth
    integer, intent(in) :: nBins

    findex=min(max(1+int(val/histBinWidth),1),nBins)
  end function findex

  subroutine writedist(hist,t0,dt,x0,dx,filename)
    real*8, allocatable, intent(inout) :: hist(:,:)
    real*8, intent(in) :: t0,dt
    real*8, intent(in) :: x0,dx
    character(len=*), intent(in) :: filename

    integer i, nxbins, ntbins

    nxbins = size(hist,1)
    ntbins = size(hist,2)

    open(999,file=filename)
    write(999,'(10E23.12)',advance='no') t0
    do i=1,ntbins
       if(sum(hist(:,i)) .gt. 0) then
          hist(:,i)=hist(:,i)/sum(hist(:,i))
       end if
       write(999,'(10E23.12)',advance='no') t0+dt*i
    end do
    write(999,*) ''

    do i=1,nxbins
       write(999,*) x0-dx/2+dx*i,hist(i,:)
    end do
    close(999)
  end subroutine writedist

  subroutine writedist1D(hist,t0,dt,filename)
    real*8, allocatable, intent(inout) :: hist(:)
    real*8, intent(in) :: t0,dt
    character(len=*), intent(in) :: filename

    integer i, ntbins

    ntbins = size(hist,1)

    open(999,file=filename)
    do i=1,ntbins
       write(999,'(10E23.12)',advance='no') t0+dt*i
    end do
    write(999,*) ''

    hist(:)=hist(:)/sum(hist(:))
    write(999,*) hist(:)
    close(999)
  end subroutine writedist1D

end module mccctest_mod
