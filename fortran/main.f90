!  -*-f90-*-  (for emacs)    vim:set filetype=fortran:  (for vim)
!
!> @author
!> Eero Hirvijoki
!
!> @brief
!> A short program for testing various things. Feel free to comment out those
!> that are not relevant for you.
!
program main
  use simpson_mod, only: simpson_test
  use special_mod, only: special_test
  use mccc_mod, only: mccc_testCoeff
  use mccctest_mod
  use wiener_mod, only: wiener_test
  implicit none

  write(*,*) "********************"
  write(*,*) "call simpson_test()"
  call simpson_test()

  write(*,*) "********************"
  write(*,*) "call special_test()"
  call special_test()

  write(*,*) "********************"
  write(*,*) "call mccc_testCoeff()"
  call mccc_testCoeff()

  write(*,*) "********************"
  write(*,*) "call wiener_test()"
  call wiener_test()

  write(*,*) "********************"
  write(*,*) "Checking that collisions result in Maxwell-Juttner distribution"
  call mccctest_MJ(10)
  write(*,*) "--------------------"
  call mccctest_MJ(11)
  write(*,*) "--------------------"
  call mccctest_MJ(21)

  write(*,*) "********************"
  write(*,*) "Checking that slowing down process is correctly modeled"
  call mccctest_SD(10)
  write(*,*) "--------------------"
  call mccctest_SD(11)
  write(*,*) "--------------------"
  call mccctest_SD(21)

  write(*,*) "Unit tests complete!"

end program main
