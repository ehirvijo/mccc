% The folder where the output files from unit tests are
pathin = '../fortran/'; 

%% Read data
PRTF_xi = importdata([pathin 'unittest_SD_PRTF_xi.out']);
PRTF_p = importdata([pathin 'unittest_SD_PRTF_p.out']);
PRTF_t = importdata([pathin 'unittest_SD_PRTF_t.out']);

PRTA_xi = importdata([pathin 'unittest_SD_PRTA_xi.out']);
PRTA_p = importdata([pathin 'unittest_SD_PRTA_p.out']);
PRTA_t = importdata([pathin 'unittest_SD_PRTA_t.out']);

GCA_xi = importdata([pathin 'unittest_SD_GCA_xi.out']);
GCA_p = importdata([pathin 'unittest_SD_GCA_p.out']);
GCA_x = importdata([pathin 'unittest_SD_GCA_x.out']);
GCA_t = importdata([pathin 'unittest_SD_GCA_t.out']);

%% Initialize
tgrid = GCA_xi(1,2:end);
xigrid = GCA_xi(2:end,1);
xgrid = GCA_x(2:end,1);
pgrid = GCA_p(2:end,1);

theta = 1e-1;

PRTF_p_mean = mean(PRTF_p(2:end,2:end).*repmat(pgrid,[1 numel(tgrid)])*numel(pgrid));
PRTF_p_var = zeros(size(tgrid));
PRTF_xi_var = zeros(size(tgrid));
PRTA_p_var = zeros(size(tgrid));
PRTA_xi_var = zeros(size(tgrid));
GCA_p_var = zeros(size(tgrid));
GCA_xi_var = zeros(size(tgrid));
for i=1:numel(tgrid)
    if ~isnan(PRTF_p(2:end,1+i))
        PRTF_p_var(i)  =  var(pgrid,PRTF_p(2:end,1+i));
        PRTF_xi_var(i) = var(xigrid,PRTF_xi(2:end,1+i));
    end
    if ~isnan(PRTA_p(2:end,1+i))
        PRTA_p_var(i)  =  var(pgrid,PRTA_p(2:end,1+i));
        PRTA_xi_var(i) = var(xigrid,PRTA_xi(2:end,1+i));
    end
    if ~isnan(GCA_p(2:end,1+i))
        GCA_p_var(i)  =  var(pgrid,GCA_p(2:end,1+i));
        GCA_xi_var(i) = var(xigrid,GCA_xi(2:end,1+i));
    end
end

PRTF_xi_mean = mean(PRTF_xi(2:end,2:end).*repmat(xigrid,[1 numel(tgrid)])*numel(xigrid));

PRTA_p_mean = mean(PRTA_p(2:end,2:end).*repmat(pgrid,[1 numel(tgrid)])*numel(pgrid));
%PRTA_p_var = var(PRTA_p(2:end,2:end).*repmat(pgrid,[1 numel(tgrid)])*numel(pgrid));
PRTA_xi_mean = mean(PRTA_xi(2:end,2:end).*repmat(xigrid,[1 numel(tgrid)])*numel(xigrid));
%PRTA_xi_var = var(PRTA_xi(2:end,2:end).*repmat(xigrid,[1 numel(tgrid)])*numel(xigrid));

GCA_p_mean = mean(GCA_p(2:end,2:end).*repmat(pgrid,[1 numel(tgrid)])*numel(pgrid));
%GCA_p_var = var(GCA_p(2:end,2:end).*repmat(pgrid,[1 numel(tgrid)])*numel(pgrid));
GCA_xi_mean = mean(GCA_xi(2:end,2:end).*repmat(xigrid,[1 numel(tgrid)])*numel(xigrid));
%GCA_xi_var = var(GCA_xi(2:end,2:end).*repmat(xigrid,[1 numel(tgrid)])*numel(xigrid));

ig = @(x,mu,lambda)( sqrt(lambda./(2*pi*x.^3)).*exp(-lambda.*(x-mu).^2./(2*mu^2*x)) );
alpha = 4;
K = 0.064*1e3;
D = 0.65;
%mu = 4/60;
%lambda = (4/1.0)^2;
mu = alpha/K;
lambda = alpha^2/(2*D);
SD_t = ig(tgrid,mu,lambda)/sum(ig(tgrid,mu,lambda));

%% Plot

% Slowing down time distribution
figure;hold on;
plot(tgrid,SD_t)
plot(tgrid,PRTF_t(2,2:end))
plot(tgrid,PRTA_t(2,2:end))
plot(tgrid,GCA_t(2,2:end))
set(gca,'fontsize',12,'box','on')
xlabel('Time (s)')
title('Slowing down time')
l=legend('Particle, F','Particle, A','Guiding center, A');
set(l,'location','northeast','fontsize',8)

SD_sd = struct();
SD_sd.tgrid = tgrid;
SD_sd.SD_t = SD_t;
SD_sd.PRTF_t = PRTF_t(2,2:end);
SD_sd.PRTA_t = PRTA_t(2,2:end);
SD_sd.GCA_t = GCA_t(2,2:end);

% Time evolution of p and xi
figure('position',[200 200 600 600])
subplot(2,2,1);hold on;
plot(tgrid,PRTF_p_mean)
plot(tgrid,PRTA_p_mean)
plot(tgrid,GCA_p_mean)
set(gca,'fontsize',12,'box','on')
xlabel('Time (s)')
title('E[p]')
 
subplot(2,2,2);hold on;
plot(tgrid,PRTF_xi_mean)
plot(tgrid,PRTA_xi_mean)
plot(tgrid,GCA_xi_mean)
set(gca,'fontsize',12,'box','on')
xlabel('Time (s)')
title('E[\xi]')

subplot(2,2,3);hold on;
plot(tgrid,PRTF_p_var)
plot(tgrid,PRTA_p_var)
plot(tgrid,GCA_p_var)
set(gca,'fontsize',12,'box','on')
xlabel('Time (s)')
title('Var[p]')
 
subplot(2,2,4);hold on;
plot(tgrid,PRTF_xi_var)
plot(tgrid,PRTA_xi_var)
plot(tgrid,GCA_xi_var)
set(gca,'fontsize',12,'box','on')
xlabel('Time (s)')
title('Var[\xi]')

l=legend('Particle, F','Particle, A','Guiding center, A');
set(l,'location','northeast','fontsize',8)

%save('~/tmp/verification_SD_sd.mat','SD_sd')
