% The folder where the output files from unit tests are
pathin = '../fortran/'; 

%% Read data
PRTF_xi = importdata([pathin 'unittest_MJ_PRTF_xi.out']);
PRTF_p = importdata([pathin 'unittest_MJ_PRTF_p.out']);

PRTA_xi = importdata([pathin 'unittest_MJ_PRTA_xi.out']);
PRTA_p = importdata([pathin 'unittest_MJ_PRTA_p.out']);

GCA_xi = importdata([pathin 'unittest_MJ_GCA_xi.out']);
GCA_p = importdata([pathin 'unittest_MJ_GCA_p.out']);
GCA_x = importdata([pathin 'unittest_MJ_PRTF_x.out']);


%% Initialize
tgrid = GCA_xi(1,2:end);
xigrid = GCA_xi(2:end,1);
xgrid = GCA_x(2:end,1);
pgrid = GCA_p(2:end,1);

theta = 1e-1;
t_thermal = 1.5e-2;
tidx = find(tgrid > t_thermal,1,'first');

%sum(rho.^2.*MJ.*pfreq)
Dc = 4.5685e-06;%(2.4166e-04^2*752)/2;% Classical diffusion coefficient rho^2*nu/2
Dmc = mean(GCA_x(2:end,end).*xgrid*numel(xgrid));
Ddistmc = GCA_x(2:end,end)/Dmc;Ddistmc = Ddistmc/sum(Ddistmc); % Diffusion coefficient distribution
Ddistc = chi2pdf(xgrid/Dc,1);Ddistc=Ddistc/sum(Ddistc); % Theoretical distribution

MJ_p = pgrid.^(2).*exp((-sqrt(1+pgrid.^2))/theta);
MJ_p = MJ_p/sum(MJ_p);
MJ_xi = ones(size(xigrid));
MJ_xi = MJ_xi/sum(MJ_xi);

MJ_p_mean = mean(MJ_p.*pgrid)*ones(size(tgrid))*numel(pgrid);
MJ_p_var  =  var(pgrid,MJ_p)*ones(size(tgrid));
MJ_xi_mean = 0*ones(size(tgrid));
MJ_xi_var = (1/3)*ones(size(tgrid));

PRTF_p_mean = mean(PRTF_p(2:end,2:end).*repmat(pgrid,[1 numel(tgrid)])*numel(pgrid));
%PRTF_p_var  =  var(PRTF_p(2:end,2:end).*repmat(pgrid,[1 numel(tgrid)])*numel(pgrid));
PRTF_xi_mean = mean(PRTF_xi(2:end,2:end).*repmat(xigrid,[1 numel(tgrid)])*numel(xigrid));
%PRTF_xi_var = var(PRTF_xi(2:end,2:end).*repmat(xigrid,[1 numel(tgrid)])*numel(xigrid));

PRTA_p_mean = mean(PRTA_p(2:end,2:end).*repmat(pgrid,[1 numel(tgrid)])*numel(pgrid));
%PRTA_p_var = var(PRTA_p(2:end,2:end).*repmat(pgrid,[1 numel(tgrid)])*numel(pgrid));
PRTA_xi_mean = mean(PRTA_xi(2:end,2:end).*repmat(xigrid,[1 numel(tgrid)])*numel(xigrid));
%PRTA_xi_var = var(PRTA_xi(2:end,2:end).*repmat(xigrid,[1 numel(tgrid)])*numel(xigrid));

GCA_p_mean = mean(GCA_p(2:end,2:end).*repmat(pgrid,[1 numel(tgrid)])*numel(pgrid));
%GCA_p_var = var(GCA_p(2:end,2:end).*repmat(pgrid,[1 numel(tgrid)])*numel(pgrid));
GCA_xi_mean = mean(GCA_xi(2:end,2:end).*repmat(xigrid,[1 numel(tgrid)])*numel(xigrid));
%GCA_xi_var = var(GCA_xi(2:end,2:end).*repmat(xigrid,[1 numel(tgrid)])*numel(xigrid));

PRTF_p_var = zeros(size(tgrid));
PRTF_xi_var = zeros(size(tgrid));
PRTA_p_var = zeros(size(tgrid));
PRTA_xi_var = zeros(size(tgrid));
GCA_p_var = zeros(size(tgrid));
GCA_xi_var = zeros(size(tgrid));
for i=1:numel(tgrid)
    if ~isnan(PRTF_p(2:end,1+i))
        PRTF_p_var(i)  =  var(pgrid,PRTF_p(2:end,1+i));
        PRTF_xi_var(i) = var(xigrid,PRTF_xi(2:end,1+i));
    end
    if ~isnan(PRTA_p(2:end,1+i))
        PRTA_p_var(i)  =  var(pgrid,PRTA_p(2:end,1+i));
        PRTA_xi_var(i) = var(xigrid,PRTA_xi(2:end,1+i));
    end
    if ~isnan(GCA_p(2:end,1+i))
        GCA_p_var(i)  =  var(pgrid,GCA_p(2:end,1+i));
        GCA_xi_var(i) = var(xigrid,GCA_xi(2:end,1+i));
    end
end

%% Plot

% p and xi dists
figure('position',[200 200 500 800])
subplot(3,1,1);hold on;
plot(pgrid,sum(PRTF_p(2:end,tidx:end),2)/sum(sum(PRTF_p(2:end,tidx:end),2)))
plot(pgrid,sum(PRTA_p(2:end,tidx:end),2)/sum(sum(PRTA_p(2:end,tidx:end),2)))
plot(pgrid,sum(GCA_p(2:end,tidx:end),2)/sum(sum(GCA_p(2:end,tidx:end),2)))
plot(pgrid,MJ_p)

set(gca,'Xlim',[0 2],'fontsize',12,'box','on')
xlabel('p/mc')
ylabel('f(p)')
l=legend('Particle, Fixed','Particle, Adaptive','Guiding center, Adaptive','Theory');
set(l,'location','northeast','fontsize',8)
title('Maxwell-Jüttner distribution')

subplot(3,1,2);hold on;
plot(pgrid,log10(sum(PRTF_p(2:end,tidx:end),2)/sum(sum(PRTF_p(2:end,tidx:end),2))))
plot(pgrid,log10(sum(PRTA_p(2:end,tidx:end),2)/sum(sum(PRTA_p(2:end,tidx:end),2))))
plot(pgrid,log10(sum(GCA_p(2:end,tidx:end),2)/sum(sum(GCA_p(2:end,tidx:end),2))))
plot(pgrid,log10(MJ_p))

set(gca,'Xlim',[0 2],'fontsize',12,'box','on')
xlabel('p/mc')
ylabel('log_{10} f(p)')

subplot(3,1,3);hold on;
plot(xigrid,sum(PRTF_xi(2:end,tidx:end),2)/sum(sum(PRTF_xi(2:end,tidx:end),2)))
plot(xigrid,sum(PRTA_xi(2:end,tidx:end),2)/sum(sum(PRTA_xi(2:end,tidx:end),2)))
plot(xigrid,sum(GCA_xi(2:end,tidx:end),2)/sum(sum(GCA_xi(2:end,tidx:end),2)))
plot(xigrid,MJ_xi)

set(gca,'Xlim',[-1 1],'fontsize',12,'box','on')
xlabel('\xi')
ylabel('f(\xi)')

MJ_dist = struct();
MJ_dist.pgrid = pgrid;
MJ_dist.MJ_p = MJ_p;
MJ_dist.PRTF_p = sum(PRTF_p(2:end,tidx:end),2)/sum(sum(PRTF_p(2:end,tidx:end),2));
MJ_dist.PRTA_p = sum(PRTA_p(2:end,tidx:end),2)/sum(sum(PRTA_p(2:end,tidx:end),2));
MJ_dist.GCA_p = sum(GCA_p(2:end,tidx:end),2)/sum(sum(GCA_p(2:end,tidx:end),2));
MJ_dist.xigrid = xigrid;
MJ_dist.MJ_xi = MJ_xi;
MJ_dist.PRTF_xi = sum(PRTF_xi(2:end,tidx:end),2)/sum(sum(PRTF_xi(2:end,tidx:end),2));
MJ_dist.PRTA_xi = sum(PRTA_xi(2:end,tidx:end),2)/sum(sum(PRTA_xi(2:end,tidx:end),2));
MJ_dist.GCA_xi = sum(GCA_xi(2:end,tidx:end),2)/sum(sum(GCA_xi(2:end,tidx:end),2));


% D-dist
figure();hold on
plot(xgrid/Dmc,log10(Ddistmc))
plot(xgrid/Dc,log10(Ddistc))

set(gca,'Xlim',[0 6],'fontsize',12,'box','on')
xlabel('x')
ylabel('f(x)')
l=legend('Guiding center, Adaptive','Theory');
set(l,'location','northeast','fontsize',8)
title('Spatial diffusion')

MJ_D = struct();
MJ_D.Dmcgrid = xgrid/Dmc;
MJ_D.Dmc = Ddistmc;
MJ_D.Dcgrid = xgrid/Dc;
MJ_D.Dc = Ddistc;

% Time evolution of p and xi
figure('position',[200 200 600 600])
subplot(2,2,1);hold on;
plot(tgrid,PRTF_p_mean)
plot(tgrid,PRTA_p_mean)
plot(tgrid,GCA_p_mean)
plot(tgrid,MJ_p_mean)
set(gca,'fontsize',12,'box','on')
xlabel('Time (s)')
title('E[p]')
 
subplot(2,2,2);hold on;
plot(tgrid,PRTF_xi_mean)
plot(tgrid,PRTA_xi_mean)
plot(tgrid,GCA_xi_mean)
plot(tgrid,MJ_xi_mean)
set(gca,'fontsize',12,'box','on')
xlabel('Time (s)')
title('E[\xi]')

subplot(2,2,3);hold on;
plot(tgrid,PRTF_p_var)
plot(tgrid,PRTA_p_var)
plot(tgrid,GCA_p_var)
plot(tgrid,MJ_p_var)
set(gca,'fontsize',12,'box','on')
xlabel('Time (s)')
title('Var[p]')
 
subplot(2,2,4);hold on;
plot(tgrid,PRTF_xi_var)
plot(tgrid,PRTA_xi_var)
plot(tgrid,GCA_xi_var)
plot(tgrid,MJ_xi_var)
set(gca,'fontsize',12,'box','on')
xlabel('Time (s)')
title('Var[\xi]')

MJ_vals = struct();
MJ_vals.tgrid = tgrid;

MJ_vals.MJ_p_mean = MJ_p_mean;
MJ_vals.PRTF_p_mean = PRTF_p_mean;
MJ_vals.PRTA_p_mean = PRTA_p_mean;
MJ_vals.GCA_p_mean = GCA_p_mean;

MJ_vals.MJ_xi_mean = MJ_xi_mean;
MJ_vals.PRTF_xi_mean = PRTF_xi_mean;
MJ_vals.PRTA_xi_mean = PRTA_xi_mean;
MJ_vals.GCA_xi_mean = GCA_xi_mean;

MJ_vals.MJ_p_var = MJ_p_var;
MJ_vals.PRTF_p_var = PRTF_p_var;
MJ_vals.PRTA_p_var = PRTA_p_var;
MJ_vals.GCA_p_var = GCA_p_var;

MJ_vals.MJ_xi_var = MJ_xi_var;
MJ_vals.PRTF_xi_var = PRTF_xi_var;
MJ_vals.PRTA_xi_var = PRTA_xi_var;
MJ_vals.GCA_xi_var = GCA_xi_var;

l=legend('Particle, F','Particle, A','Guiding center, A','Theory');
set(l,'location','northeast','fontsize',8)

%save('/tmp/verification_MJ_dist.mat','MJ_dist')
%save('/tmp/verification_MJ_D.mat','MJ_D')
%save('/tmp/verification_MJ_vals.mat','MJ_vals')
