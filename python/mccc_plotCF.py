##
# This script plots the collision coefficients along with the derivatives and their numerical approximations.
# 
# Konsta Sarkimaki
# konsta.sarkimaki@aalto.fi
#
##

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# Import and initialize data
data = np.loadtxt("test_coefficients.out")

u = data[0:-1,0]
th = data[0,1]
K = data[0:-1,2]
dK = data[0:-1,3]
Dpar = data[0:-1,4]
dDpar = data[0:-1,5]
Dperp = data[0:-1,6]
dDperp = data[0:-1,7]
kappa = data[0:-1,8]
dkappa = data[0:-1,9]

plt.ion()
plt.figure()
plt.subplot(431)
plt.plot(u,K)
plt.subplot(432)
plt.plot(u,dK)
plt.subplot(433)
plt.plot(u[1:-1],abs(dK[0:-2]-(K[0:-2]-K[1:-1])/(u[0:-2]-u[1:-1]) )/abs(dK[0:-2]))
plt.ylim([0, 0.1])

plt.subplot(434)
plt.plot(u,kappa)
plt.subplot(435)
plt.plot(u,dkappa)
plt.subplot(436)
plt.plot(u[1:-1],abs(dkappa[0:-2]-(kappa[0:-2]-kappa[1:-1])/(u[0:-2]-u[1:-1]) )/abs(dkappa[0:-2]))
plt.ylim([0, 1])

plt.subplot(437)
plt.plot(u,Dpar)
plt.subplot(438)
plt.plot(u,dDpar)
plt.subplot(439)
plt.plot(u[1:-1],abs(dDpar[0:-2]-(Dpar[0:-2]-Dpar[1:-1])/(u[0:-2]-u[1:-1]) )/abs(dDpar[0:-2]))
plt.ylim([0, 0.1])

plt.subplot(4,3,10)
plt.plot(u,Dperp)
plt.subplot(4,3,11)
plt.plot(u,dDperp)
plt.subplot(4,3,12)
plt.plot(u[1:-1],abs(dDperp[0:-2]-(Dperp[0:-2]-Dperp[1:-1])/(u[0:-2]-u[1:-1]) )/abs(dDperp[0:-2]))
plt.ylim([0, 0.1])

plt.show(block=True)
