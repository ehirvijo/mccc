##
# This script plots the slowing down distributions computed by "mccctest" module.
# 
# Konsta Sarkimaki
# konsta.sarkimaki@aalto.fi
#
##

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# Import and initialize data
PRTF_t = np.loadtxt("unittest_SD_PRTF_t.out")
PRTA_t = np.loadtxt("unittest_SD_PRTA_t.out")
GCA_t = np.loadtxt("unittest_SD_GCA_t.out")

tgrid = GCA_t[0,0:-1]

# Inverse Gaussian distribution x ~ IG(mu,lambd)
ig = lambda x,mu,lambd: np.sqrt(lambd/(2*np.pi*x**3))*np.exp(-lambd*(x-mu)**2/(2*mu**2*x))

# Simulation specific values
alpha = 4
K = 0.064*1e3
D = 0.65

mu = alpha/K;
lambd = alpha**2/(2*D);
SD_t = ig(tgrid,mu,lambd)/sum(ig(tgrid,mu,lambd));

plt.ion()
plt.figure()
h1, = plt.plot(tgrid,PRTF_t[1,0:-1],color='blue')
h2, = plt.plot(tgrid,PRTA_t[1,0:-1],color='red')
h3, = plt.plot(tgrid,GCA_t[1,0:-1],color='green')
h4, = plt.plot(tgrid,SD_t,color='purple')
plt.xlabel(r'Slowing down time (s)')
plt.ylabel(r'$f(t)$')
plt.xlim([0.04, 0.08])
plt.legend([h1,h2,h3,h4],['FEP','AMP','AMG','Analytical'])

plt.show(block=True)
