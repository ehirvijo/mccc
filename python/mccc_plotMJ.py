##
# This script plots the equilibrium distributions computed by "mccctest" module.
# 
# Konsta Sarkimaki
# konsta.sarkimaki@aalto.fi
#
##

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from scipy.stats import chi2

# Import and initialize data
PRTF_xi = np.loadtxt("unittest_MJ_PRTF_xi.out")
PRTF_p = np.loadtxt('unittest_MJ_PRTF_u.out')
PRTF_x = np.loadtxt('unittest_MJ_PRTF_x.out')

PRTA_xi = np.loadtxt('unittest_MJ_PRTA_xi.out')
PRTA_p = np.loadtxt('unittest_MJ_PRTA_u.out')
PRTA_x = np.loadtxt('unittest_MJ_PRTA_x.out')

GCA_xi = np.loadtxt('unittest_MJ_GCA_xi.out')
GCA_p = np.loadtxt('unittest_MJ_GCA_u.out')
GCA_x = np.loadtxt('unittest_MJ_GCA_x.out')


tgrid = GCA_xi[0,1:-1]
xigrid = GCA_xi[1:-1,0]
xgrid = GCA_x[1:-1,0]
pgrid = GCA_p[1:-1,0]

# The analytical solution for the distribution
theta = 1e-1
MJ_p = pgrid**2*np.exp(-np.sqrt(1+pgrid**2)/theta)
MJ_p = MJ_p/np.sum(MJ_p)
MJ_xi = np.ones(xigrid.size)
MJ_xi = MJ_xi/np.sum(MJ_xi)

# Set the time after which the population seems to be thermalized
# and find the corresponding index in the time grid. After this
# index, the distributions are summed to obtain the equilibrium profile.
t_thermal = 1.5e-2
tidx = np.where(tgrid > t_thermal)
tidx = tidx[0][0]

eq_PRTF_p = np.sum(PRTF_p[1:-1,tidx:-1], axis=1)/np.sum(np.sum(PRTF_p[1:-1,tidx:-1], axis=1))
eq_PRTA_p = np.sum(PRTA_p[1:-1,tidx:-1], axis=1)/np.sum(np.sum(PRTA_p[1:-1,tidx:-1], axis=1))
eq_GCA_p = np.sum(GCA_p[1:-1,tidx:-1], axis=1)/np.sum(np.sum(GCA_p[1:-1,tidx:-1], axis=1))

eq_PRTF_xi = np.sum(PRTF_xi[1:-1,tidx:-1], axis=1)/np.sum(np.sum(PRTF_xi[1:-1,tidx:-1], axis=1))
eq_PRTA_xi = np.sum(PRTA_xi[1:-1,tidx:-1], axis=1)/np.sum(np.sum(PRTA_xi[1:-1,tidx:-1], axis=1))
eq_GCA_xi = np.sum(GCA_xi[1:-1,tidx:-1], axis=1)/np.sum(np.sum(GCA_xi[1:-1,tidx:-1], axis=1))

plt.ion()
plt.figure()
plt.subplot(311)
h1, = plt.plot(pgrid,eq_PRTF_p,color='blue')
h2, = plt.plot(pgrid,eq_PRTA_p,color='red')
h3, = plt.plot(pgrid,eq_GCA_p,color='green')
h4, = plt.plot(pgrid,MJ_p,color='purple')
plt.xlabel(r'$u$')
plt.ylabel(r'$f(u)$')
plt.xlim([0, 3])
plt.legend([h1,h2,h3,h4],['FEP','AMP','AMG','Analytical'])

plt.subplot(312)
h1, = plt.plot(xigrid,eq_PRTF_xi,color='blue')
h2, = plt.plot(xigrid,eq_PRTA_xi,color='red')
h3, = plt.plot(xigrid,eq_GCA_xi,color='green')
h4, = plt.plot(xigrid,MJ_xi,color='purple')
plt.xlabel(r'$\xi$')
plt.ylabel(r'$f(\xi)$')

plt.subplot(313)
h1, = plt.plot(xgrid,PRTF_x[1:-1,-1],color='blue')
h2, = plt.plot(xgrid,PRTA_x[1:-1,-1],color='red')
h3, = plt.plot(xgrid,GCA_x[1:-1,-1],color='green')
plt.xlabel(r'$z$')
plt.ylabel(r'$f(z)$')

plt.show(block=True)
