\documentclass[a4paper]{article}

\usepackage[T1]{fontenc}
\usepackage{xcolor}
\usepackage{lmodern}
\usepackage{listings}
\usepackage[margin=2.5cm]{geometry}

\lstset{language=[90]Fortran,
  basicstyle=\ttfamily\scriptsize,
  keywordstyle=\color{red},
  commentstyle=\color{blue},
  morecomment=[l]{!\ }% Comment only with space after !
}

\newcommand{\code}[1]{\emph{#1}}
\newcommand{\tag}[1]{\emph{AMCC}}

\setlength\parindent{0pt}

\title{AMCC documentation}

\begin{document}
\maketitle
This is the documentation for the Adaptive Monte Carlo Coulomb collisions (\tag{}) code. \tag{} solves test particle collisions with background obeying Maxwell-J\"uttner statistics. The program is written in Fortran 95. \tag{} is not intended for stand-alone use but to be used within Monte Carlo codes requiring efficient and robust collision operator. However, a test program is provided for verifying that the collision operators lead to known analytical results.

\subsubsection*{Contact}
Konsta S\"arkim\"aki\\konsta.sarkimaki@aalto.fi\\Aalto University, Espoo, Finland\\
\\
Eero Hirvijoki\\ehirvijo@pppl.gov\\Princeton Plasma Physics Laboratory, Princeton, NJ, USA

\section*{Features}

\tag{} features Monte Carlo collision operators that integrate the particle state when the particle interacts with the given plasma background for a given time.
\tag{} is a fully relativistic code meaning both test particle and plasma can be relativistic.
The Monte Carlo method is implemented for both the three-dimensional particle momentum space, $(u_x,u_y,u_z)$, and the five-dimensional guiding center phase space , $(X_x,X_y,X_z,u,\xi)$.
The integration method supports both fixed and adaptive time step schemes.
In latter, the integrator chooses the time step so that user-set tolerances are obeyed.
In addition to collision operators, a simple algorithm for solving particle trajectory due to the Lorentz force is included. The code is not parallelized.
\\
\\
\tag{} uses SI units with two exceptions:
\begin{itemize}
\item{Momentum is normalized as $\mathbf{u} \equiv \mathbf{p}/m_a c$ where $m_a$ is test particle mass, $\mathbf{p}$ test particle momentum, and $c$ the speed of light.}
\item{Temperature is normalized as $\Theta_b \equiv T_b/m_bc^2$ where $m_b$ is background species mass and $T_b$ is background temperature.}
\end{itemize}

\section*{Public functions}
\begin{lstlisting}

module mccc_mod:
	
  !> Initializes the mccc_special struct that stores the data required for
  !> interpolating integrals L0 and L1. The interpolation grid is
  !> logarithmic and uses linear interpolation. Deallocate with mccc_uninit().
  !>
  !> input:
  !>
  !> real*8  uminxp      -- defines minimum u as umin=10^uminxp
  !> real*8  umaxxp      -- defines maximum u as umax=10^umaxxp
  !> real*8  thminxp     -- defines minimum theta as thmin=10^thminxp
  !> real*8  thmaxxp     -- defines maximum theta as thmax=10^thmaxxp
  !> integer nu          -- number of u grid points
  !> integer nth         -- number of theta grid points
  !> real*8  L0L1_eps    -- (optional) 
  !> real*8  L0L1_cutoff -- (optional) 
  !>
  !> output:
  !> 
  !> mccc_special        -- interpolation data
  !<
  type(mccc_special) function mccc_init(uminxp,umaxxp,thminxp,thmaxxp,nu,nth,&
       L0L1_eps,L0L1_cutoff)
       
  !> Deinitializes the mccc_special struct.
  !>
  !> input:
  !>
  !> mccc_special dat -- interpolation data
  !<
  integer function mccc_uninit(dat)
  
  !> Writes the mccc_special struct to a file.
  !>
  !> input:
  !>
  !> mccc_special dat      -- interpolation data
  !> character(len=*) fn   -- output filename
  !>
  !> output:
  !>
  !> integer mccc_writedat -- negative if there was an error
  !<
  integer function mccc_writedat(dat,fn)
  
  !> Reads the mccc_special struct from a file.
  !>
  !> input:
  !>
  !> character(len=*) fn -- input filename
  !>
  !> output
  !>
  !> mccc_special        -- interpolation data
  !<
  type(mccc_special) function mccc_readdat(fn)
	
  !> Computes Coulomb logarithm for a given test particle and plasma species.
  !> The logarithm is estimated as ln{lambda_D/min{bqm,bcl}} where lambda_D is
  !> the Debye length, and bqm and bcl are quantum mechanical and classical
  !> impact parameters, respectively. The Coulomb logarithm for different
  !> plasma species is returned.
  !>
  !> input:
  !> 
  !> mccc_special dat -- mccc_special struct that is obtained with the mccc_init() call
  !> real*8 ma        -- test particle mass [kg]
  !> real*8 qa        -- test particle charge [C]
  !> real*8 mb(:)     -- list of background species masses [kg]
  !> real*8 qb(:)     -- list of background species charges [C]
  !> real*8 nb(:)     -- list of background densities [1/m^3]
  !> real*8 thb(:)    -- list of normalized background temperatures thb=T_b/(m_b*c^2) [1] 
  !> real*8 u         -- normalized test particle momentum magnitude u=|p|/mc [1]
  !>
  !> output:
  !>
  !> real*8 clogab(:) -- list of Coulomb logarithms for species a colliding with b [1] 
  !<
  subroutine mccc_clog(ma,qa,mb,qb,nb,thb,u,clog)
  
  !> Computes requested collision coefficients and respective derivatives
  !> for a given test particle and background species.
  !> 
  !> input:
  !> 
  !> mccc_special dat -- mccc_special struct that is obtained with the mccc_init() call
  !> real*8 ma        -- test particle mass [kg]
  !> real*8 qa        -- test particle charge [C]
  !> real*8 clogab    -- coulomb logarithm for species a colliding with b [1] 
  !> real*8 mb        -- background species mass [kg]
  !> real*8 qb        -- background species charge [C]
  !> real*8 nb        -- background density [1/m^3]
  !> real*8 thb       -- normalized background temperature thb=T_b/(m_b*c^2) [1] 
  !> real*8 u         -- normalized test particle momentum magnitude u=|p|/mc [1]
  !>
  !> output:
  !> 
  !> real*8 K         -- (optional) friction coefficient [1/s]
  !> real*8 dK        -- (optional) derivative of K with respect to u [1/s]
  !> real*8 Dpar      -- (optional) parallel momentum diffusion [1/s]
  !> real*8 dDpar     -- (optional) derivative of Dpar with respect to u [1/s]
  !> real*8 Dperp     -- (optional) perpendicular momentum diffusion [1/s]
  !> real*8 dDperp    -- (optional) derivative of Dperp with respect to u [1/s]
  !> real*8 kappa     -- (optional) guiding center friction coefficient [1/s]
  !> real*8 dkappa    -- (optional) derivative of kappa with respect to u [1/s]
  !<
  subroutine mccc_coeffs(dat,ma,qa,clogab,mb,qb,nb,thb,u,&
       K,dK,Dpar,dDpar,Dperp,dDperp,kappa,dkappa)

  !> Computes the value for particle momentum after collisions with
  !> background species using Euler-Maruyama method with a fixed time step.
  !> 
  !> input:
  !> 
  !> mccc_special dat  -- mccc_special struct that is obtained with the mccc_init() call
  !> real*8  ma        -- test particle mass [kg]
  !> real*8  qa        -- test particle charge [C]
  !> real*8  clogab(:) -- list of coulomb logarithms for species a colliding with b [1] 
  !> real*8  mb(:)     -- list of background species masses [kg]
  !> real*8  qb(:)     -- list of background species charges [C]
  !> real*8  nb(:)     -- list of background densities [1/m^3]
  !> real*8  thb(:)    -- list of normalized background temperatures thb=T_b/(m_b*c^2) [1] 
  !> real*8  dt        -- time step length [s]
  !> real*8  rnd(3)    -- array with three elements of standard normal random numbers ~ N(0,1)
  !> real*8  uin(3)    -- normalized test particle momentum u=p/mc before collisions [1]
  !>
  !> output:
  !> 
  !> real*8  uout(3)   -- normalized test particle momentum u=p/mc after collisions [1]
  !> integer err       -- error flag, negative indicates something went wrong
  !<
  subroutine mccc_push(dat,ma,qa,clogab,mb,qb,nb,thb,dt,rnd,uin,uout,err)
  
  !> Prints error diagnosis if an error has occurred.
  !>
  !> input:
  !>
  !> integer err -- error flag, negative indicates something went wrong
  !<
  subroutine mccc_error(err)
\end{lstlisting}
\begin{lstlisting}
module wiener_mod:

  !> Allocates an array that will be used to store generated Wiener processes until
  !> they are no longer needed. Deallocation is done with subroutine wiener_deallocate.
  !> Array has format (Ndim+2) X Nslots where the first row indicates the position of 
  !> the next process in time or its own position if no such process exists. The first
  !> process is always located at the firs column. The second row stores the time 
  !> instants processes correspond. The rest of the rows stores values of the Ndim
  !> dimensional Wiener processes. Nslots value should not be overly large.
  !>
  !> input:
  !>
  !> integer Ndim         -- Number of Wiener process dimensions
  !> integer Nslots       -- Maximum number of time instances for which processes are stored.
  !> real*8  initime      -- Time (>0) at the beginning of the simulation [s]
  !>
  !> output:
  !> 
  !> real*8  wienarr(:,:) -- array that stores the Wiener processes
  !<
  subroutine wiener_allocate(wienarr, Ndim, Nslots, initime)
  	
  !> Deallocates the array storing the Wiener processes.
  !>
  !> input:
  !>
  !> real*8  wienarr(:,:) -- array that stores the Wiener processes
  !<
  subroutine wiener_deallocate(wienarr)
  	
  !> Generates a new Wiener process at a given time instant taking the Brownian bridge
  !> into account if needed.
  !>
  !> input:
  !>
  !> real*8  t            -- time for which the new process will be generated
  !> integer windex       -- index of the generated Wiener process in the Wiener array
  !>
  !> output:
  !> 
  !> real*8  wienarr(:,:) -- array that stores the Wiener processes
  !> integer err          -- error flag, negative indicates something went wrong
  !<
  subroutine wiener_generate(wienarr, t, windex,err)
  	
  !> Removes Wiener processes from the array that are no longer required. Processes W(t')  
  !> are redundant if t' <  t, where t is the current simulation time. Note that W(t) 
  !> should exist before W(t') are removed. This routine should be called each time when 
  !> simulation time is advanced.
  !>
  !> input:
  !>
  !> real*8  t            -- time for which the new process will be generated
  !>
  !> output:
  !> 
  !> real*8  wienarr(:,:) -- array that stores the Wiener processes
  !> integer err          -- error flag, negative indicates something went wrong
  !<
  subroutine wiener_clean(wienarr,t,err)
  	
  !> Generates standard normally distributed random numbers using the geometric Box-Muller
  !> method.
  !>
  !> output:
  !>
  !> real*8 randVar(:) -- array that is to be populated with random numbers
  !<
  subroutine wiener_boxmuller(randVar)
  
  !> Prints error diagnosis if an error has occurred.
  !>
  !> input:
  !>
  !> integer err -- error flag, negative indicates something went wrong
  !<
  subroutine wiener_error(err)
\end{lstlisting}
\begin{lstlisting}
module mccca_mod:

  !> Computes the collision coefficients and stores them in a struct that is used
  !> by mccca_push(). The coefficients are calculated separately like this so that
  !> they are not unnecessarily re-calculated if the time step is rejected.
  !> 
  !> input:
  !> 
  !> mccc_special dat       -- struct that is obtained with the mccc_init() call
  !> real*8 ma              -- test particle mass [kg]
  !> real*8 qa              -- test particle charge [C]
  !> real*8 clogab(:)       -- list of coulomb logarithms for species a colliding with b [1] 
  !> real*8 mb(:)           -- list of background species masses [kg]
  !> real*8 qb(:)           -- list of background species charges [C]
  !> real*8 nb(:)           -- list of background densities [1/m^3]
  !> real*8 thb(:)          -- list of normalized background temperatures thb=T_b/(m_b*c^2) [1] 
  !> real*8 u               -- normalized test particle momentum magnitude u=|p|/mc [1]
  !>
  !> output:
  !> 
  !> mccca_coefstruct coefs -- struct containing the coefficients needed for evaluating the collisions
  !<
  subroutine mccca_evalCoefs(dat,ma,qa,clogab,mb,qb,nb,thb,u,coefs)
  
  !> Computes the value for particle momentum after collisions with background
  !> species using Milstein method with a fixed or an adaptive time step.
  !> Returns a suggestion for the next time step. A negative suggestion indicates 
  !> the current time step was rejected, and absolute value should be used for the 
  !> next try. If no tolerance is given, Milstein method with fixed time step will
  !> be used instead. Wiener processes are generated automatically and only cleaning
  !> is required after each accepted time step.
  !>
  !> input:
  !> 
  !> mccca_coefstruct coefs -- struct containing the coefficients from mccca_evalCoefs() call
  !> real*8  wienarr(5,:)   -- array that stores the Wiener processes (Ndim = 3)
  !> real*8  uin(3)         -- normalized test particle momentum u=p/mc before collisions [1]
  !> real*8  dtin           -- candidate time step length [s]
  !> real*8  tol            -- (optional) relative tolerance. Fixed time step is used if not given
  !>
  !> output:
  !> 
  !> real*8  uout(3) -- normalized test particle momentum u=p/mc after collisions [1]
  !> real*8  dtout   -- suggestion for the next time step. dtout = dtin in case fixed step is used [s]
  !> integer err     -- error flag, negative value indicates error
  !<
  subroutine mccca_push(coefs,wienarr,uin,uout,dtin,dtout,tol,err) 
  	
  !> Prints error diagnosis if an error has occurred.
  !>
  !> input:
  !>
  !> integer err -- error flag, negative indicates something went wrong
  !<
  subroutine mccca_error(err)
\end{lstlisting}
\begin{lstlisting}
module mcccgc_mod:

  !> Computes the collision coefficients and stores them in a struct that is used
  !> by mcccgc_push(). The coefficients are calculated separately like this so that
  !> they are not unnecessarily re-calculated if the time step is rejected.
  !> 
  !> input:
  !> 
  !> mccc_special dat -- struct that is obtained with the mccc_init() call
  !> real*8 ma        -- test particle mass [kg]
  !> real*8 qa        -- test particle charge [C]
  !> real*8 clogab(:) -- list of coulomb logarithms for species a colliding with b [1] 
  !> real*8 mb(:)     -- list of background species masses [kg]
  !> real*8 qb(:)     -- list of background species charges [C]
  !> real*8 nb(:)     -- list of background densities [1/m^3]
  !> real*8 thb(:)    -- list of normalized background temperatures thb=T_b/(m_b*c^2) [1] 
  !> real*8 u         -- normalized guiding center momentum magnitude u=p/mc [1]
  !> real*8 xi        -- guiding center pitch [1]
  !>
  !> output:
  !> 
  !> mcccgc_coefstruct coefs -- struct containing the coefficients needed for evaluating the collisions
  !<
  subroutine mcccgc_evalCoefs(dat,ma,qa,clogab,mb,qb,nb,thb,u,xi,coefs)
	
  !> Computes the value for guiding center phase space position after collisions with
  !> background species using Milstein method with a fixed or an adaptive time step.
  !> Returns a suggestion for the next time step. A negative suggestion indicates 
  !> the current time step was rejected, and absolute value should be used for the 
  !> next try. If no tolerance is given, Milstein method with fixed time step will
  !> be used instead. Wiener processes are generated automatically and only cleaning
  !> is required after each accepted time step.
  !>
  !> input:
  !> 
  !> mcccgc_coefstruct coefs -- struct containing the coefficients from mccca_evalCoefs() call
  !> real*8 wienarr(7,:)     -- array that stores the Wiener processes (Ndim=5)
  !> real*8 B(3)             -- magnetic field at guiding center position before collisions [T]
  !> real*8 cutoff           -- value (>= 0) used to mirror u as "u = 2*cutoff - u" if u < cutoff otherwise [1]
  !> real*8 uin              -- normalized test particle momentum u=p/mc before collisions [1]
  !> real*8 xiin             -- guiding center pitch before collisions [1]
  !> real*8 dtin             -- candidate time step length [s]
  !> real*8 tol(2)           -- (optional) two-element array containing the relative tolerance for the
  !>                            guiding center momentum and absolute tolerance for the pitch. Fixed time 
  !>                            step is used if not given
  !>
  !> output:
  !> 
  !> real*8 uout  -- normalized guiding center momentum u=p/mc after collisions [1]
  !> real*8 xiout -- guiding center pitch after collisions [1]
  !> real*8 dx(3) -- change in guiding center position due to collisions [m]
  !> real*8 dtout -- suggestion for the next time step. dtout = dtin in case fixed step is used [s]
  !> integer err  -- error flag, negative value indicates error
  !<
  subroutine mcccgc_push(coefs,wienarr,B,cutoff,uin,uout,xiin,xiout,dx,dtin,dtout,tol,err)
	
  !> Prints error diagnosis if an error has occurred.
  !>
  !> input:
  !>
  !> integer err -- error flag, negative indicates something went wrong
  !<
  subroutine mcccgc_error(err)
\end{lstlisting}
\begin{lstlisting}
module orbit_mod:

  !> Computes new position and momentum of a test particle in an electromagnetic
  !> field after a given time.
  !> 
  !> input:
  !> 
  !> real*8 ma        -- test particle mass [kg]
  !> real*8 qa        -- test particle charge [C]
  !> real*8 E0half(3) -- electric field at x(t0+0.5*dt) [V/m]
  !> real*8 B0half(3) -- magnetic field at x(t0+0.5*dt) [T]
  !> real*8 dt        -- time step length [s]
  !> real*8 xhalf(3)  -- test particle position at t = t0+0.5*dt (see orbit_halfpush) [m]
  !> real*8 uin(3)    -- normalized test particle momentum u=p/mc at t = t0 [1]
  !>
  !> output:
  !>
  !> real*8 xout(3)   -- test particle position at t = t0 + dt [m]
  !> real*8 uout(3)   -- normalized test particle momentum u=p/mc at t = t0 + dt [1]
  !> integer err      -- error flag, negative indicates something went wrong
  !<
  subroutine orbit_push(ma,qa,E0half,B0half,dt,xhalf,xout,uin,uout,err)

  !> Computes the particle position at t = t0+0.5*dt when dt, x(t0) and u(t0) are given.
  !> 
  !> input:
  !> 
  !> real*8 dt       -- time step length [s]
  !> real*8 x(3)     -- test particle position at t = t0 [m]
  !> real*8 u(3)     -- normalized test particle momentum u=p/mc at t = t0 [1]
  !>
  !> output:
  !>
  !> real*8 xhalf(3) -- test particle position at t = t0 + 0.5*dt [m]
  !> integer err     -- error flag, negative indicates something went wrong
  !<
  subroutine orbit_halfpush(dt,x,u,xhalf,err)
	
  !> Prints error diagnosis if an error has occurred.
  !>
  !> input:
  !>
  !> integer err -- error flag, negative indicates something went wrong
  !<
  subroutine orbit_error(err)
\end{lstlisting}

\section*{Usage}

The collision coefficients are evaluated by interpolating data which first need to be either initialized or read from file.
The following example illustrates how to read the data from \emph{mccc.data} file or, if the file does not exists, calculate the data numerically and then store it in file.
\begin{lstlisting}
use mccc_mod, only mccc_init

...

! Flag indicating whether the interpolation data already exists
logical :: storage_file_on_disk

! For input/output
integer, parameter :: chn=989

! File containing the interpolation data
character(20), parameter :: storage_file='mccc.data' 

inquire(file=storage_file,exist=storage_file_on_disk)
if(storage_file_on_disk) then
	dat = mccc_readdat(storage_file)
else
	dat=mccc_init(uminxp,umaxxp,thminxp,thmaxxp,nu,nth)
	err = mccc_writedat(dat,storage_file)
end if

...

err = mccc_deinit(dat)
\end{lstlisting}
Once the interpolation data exists, the fixed time step loop, with the orbit step included, can be implemented as
\begin{lstlisting}
use wiener_mod, only wiener_boxmuller
use mccc_mod
use orbit_mod

...

! Simulation time [s]
time = 0.D0

! Maximum time [s]
tmax = 1.D0

do while(time .lt. tmax)
	! Update Coulomb logarithm since |u| has changed
	call mccc_clog(ma,qa,mb,qb,nb,thb,norm2(uin),clog)
	
	! Generate Wiener processes for this step	
	call wiener_boxmuller(rnd)
	
	! Evaluate new momentum after the collisions
	call mccc_push(dat,ma,qa,clogab,mb,qb,nb,thb,dt,rnd,uin,uout,err)
	call mccc_error(err)
	
	! Evaluate the particle orbit
	uorb = uout! Temporary variable
	call orbit_halfpush(dt,xin,uorb,xhalf,err)
    call orbit_error(err)
    call orbit_push(ma,qa,E,B,dt,xhalf,xout,uorb,uout,err)
    call orbit_error(err)
	
	xin = xout
	uin = uout
	time = time+dt
end do
\end{lstlisting}
The adaptive loop can be implemented as
\begin{lstlisting}
use wiener_mod
use mccc_mod, only mccc_clog
use mccca_mod

...
! Simulation time [s]
real*8, time = 0.D0

! Maximum time [s]
real*8, tmax = 1.D0

! Simulation tolerance
real*8, tol = 1.D-1

! Allocate the wiener array. Nslots = 30 is a good value to start with
call wiener_allocate(wienarr,3, Nslots,time)

do while(time .lt. tmax)
          
	! Update Coulomb logarithm since |u| has changed
	call mccc_clog(ma,qa,mb,qb,nb,thb,norm2(uin),clog)

	! Evaluate the collision coefficients for this time step
	call mccca_evalCoefs(dat,ma,qa,clogab,mb,qb,nb,thb,norm2(uin),coefs)

	! Iterate until the time step is accepted
	dtout = -1.D0
	do while(dtout .lt. 0)
		! Try to evaluate the new momentum after the collisions
		call mccca_push(coefs,wienarr,uin,uout,dtin,dtout,tol,err)
		call mccc_error(err)

		! Check whether the current step was accepted
		if(dtout .gt. 0) then
			time = time+dtin
			uin = uout

			! Clear redundant Wiener processes
			call wiener_clean(wienarr,time,err)
			call wiener_error(err)
		end if
             
	dtin = abs(dtout)
end do

! Deallocate Wiener array once simulation is complete
call wiener_deallocate(wienarr)
\end{lstlisting}
The adaptive guiding center collision loop is implemented as
\begin{lstlisting}
use wiener_mod
use mccc_mod, only mccc_clog
use mcccgc_mod

...

! Simulation time [s]
real*8, time = 0.D0

! Maximum time [s]
real*8, tmax = 1.D0

! Simulation tolerance
real*8, tol(2) = [1.D-1, 1.D-1]

! Guiding center position
real*8, x(3) = [0.D0, 0.D0, 0.D0]

! Allocate the wiener array. Nslots = 30 is a good value to start with
call wiener_allocate(wienarr,5, Nslots,time)

do while(time .lt. tmax)
          
	! Update Coulomb logarithm since u has changed
	call mccc_clog(ma,qa,mb,qb,nb,thb,uin,clog)

	! Evaluate the collision coefficients for this time step
	call mcccgc_evalCoefs(dat,ma,qa,clogab,mb,qb,nb,thb,uin,xiin,coefs)

	! Iterate until the time step is accepted
	dtout = -1.D0
	do while(dtout .lt. 0)
		! Try to evaluate the new momentum after the collisions
		call mcccgc_push(coefs,wienarr,B,cutoff,uin,uout,xiin,xiout,dx,dtin,dtout,tol,err)
		call mcccgc_error(err)

		! Check whether the current step was accepted
		if(dtout .gt. 0) then
			time = time+dtin
			x = x + dx
			uin = uout
			xiin = xiout

			! Clear redundant Wiener processes
			call wiener_clean(wienarr,time,err)
			call wiener_error(err)
		end if
             
	dtin = abs(dtout)
end do

! Deallocate Wiener array once simulation is complete
call wiener_deallocate(wienarr)
\end{lstlisting}

\section*{Test program}

The routines to test the collision operators are found within \code{mccctest} module.
This module is also a good place to start when familiarizing oneself of how to use the code.
There are several other tests as well to verify internal functionality.
All test can be toggled in the program module \code{main}.
The program is compiled and run with command (in Linux terminal)
\begin{lstlisting}
make main && ./main
\end{lstlisting}
The program takes about 10 minutes to complete.
There are two test cases: simulation of thermal electrons in electron plasma for a certain time to verify that the Maxwell-J\"uttner distribution is reproduced, and simulation of fast electrons to verify that the slowing down process is correctly modelled.
The tests are performed for the three collision operators whose usage was detailed.
\\
\\
The output consists of momentum (magnitude), pitch distributions where time is the second coordinate, and a spatial (1D on an axis perpendicular to the magnetic field) distribution.
The slowing down simulation also produces a slowing down time distribution.
The output is written in ASCII files with \code{.out} extension.
\\
\\
The simulation results can be plotted with the python scripts that are provided:\\
\\ 
\code{mccc\_plotMJ.py} plots the numerical distribution function along with the analytical Maxwell-J\"uttner distribution.\\
\\
\code{mccc\_plotSD.py} plots the numerical slowing down time distribution function along with the analytical estimate.

\section*{Acknowledgements}

The code uses libraries that evaluate Bessel functions written by John Burkardt. The interpolation routines are courtesy of David Pfefferl\'e.

\end{document}