# README #

Downloaded with the command
git clone https://ehirvijo@bitbucket.org/ehirvijo/mccc.git

* To read the manual (requires pdflatex):
  cd doc/
  make

* To compile doxygen overview (requires doxygen):
  cd fortran
  make dox
 
* To compile the code (requires gfortran):
  cd fortran/
  make main

* To run unit tests:
  cd fortran/
  ./main